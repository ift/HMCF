Installation
============


In the following, we assume a Unix-like system with Python3>=3.5.1 and pip>=9.0.1 already installed.
NIFTy4 also requires the libfftw3-dev library.
HMCF and all its required dependencies (including NIFTy) can be installed via pip:

    pip install git+https://gitlab.mpcdf.mpg.de/ift/HMCF

HMCF demos rely additionally on matplotlib

    pip install matplotlib

and HMC tools also needs PyQt5

    pip install pyqt5
