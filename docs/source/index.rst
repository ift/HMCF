HMC -- Hamilton Monte Carlo Sampling for Fields
===============================================

HMCF implements a Hamiltonian Monte Carlo (HMC) sampler [1]_ for the NIFTy [2]_ (“Numerical Information Field Theory”)
framework.
It is available for Python3 on Unix-like systems.

HMCF offers an easy to use sampler if an appropriate NIFTy Energy is already available.
This can help estimating the impact of approximations present in other approaches, or enable tackling entirely new problems.

References
----------
.. [1] Duane S, Kennedy AD, Pendleton BJ, Roweth D (1987). “Hybrid monte carlo.” Physics letters B, 195(2), 216–222.
.. [2] Steininger et al., "NIFTy 3 - Numerical Information Field Theory - A Python framework for multicomponent signal inference on HPC clusters", 2017, submitted to PLOS One; `[arXiv:1708.01073] <https://arxiv.org/abs/1708.01073>`_

Contents
........

.. toctree::
   :maxdepth: 2

   installation

Indices and tables
..................

* :any:`Module Index <mod/modules>`
* :ref:`search`

