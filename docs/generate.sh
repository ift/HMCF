rm -rf docs/build docs/source/mod

python3 setup.py install --user

sphinx-apidoc -l -e -d 2 -o docs/source/mod hmcf
sphinx-build -b html docs/source/ docs/build/ -vvv
