# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

from setuptools import setup, find_packages

setup(name='hmcf',
      author='Christoph Lienhard',
      author_email='christoph.lienhard@tum.de',
      version='1.0',
      packages=find_packages(include=['hmcf', 'hmcf.*']),
      description="",
      url="https://gitlab.mpcdf.mpg.de/ift/HMCF",
      zip_safe=True,
      dependency_links=["git+https://gitlab.mpcdf.mpg.de/ift/NIFTy.git@NIFTy_4#egg=nifty4-4.2"],
      install_requires=["numpy>=1.10", "scipy>=0.17", "h5py>=2.5.0", "nifty4>=4.2"],
      license="GPLv3")
