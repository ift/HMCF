HMCF - Hamiltonian Monte Carlo Sampling for Fields
===============================================

Summary
-------

HMCF “Hamiltonian Monte Carlo for Fields” is a software add-on for the NIFTy 
“Numerical Information Field Theory” framework implementing Hamiltonian Monte 
Carlo (HMC) sampling in Python. HMCF as well as NIFTy are designed to address 
inference problems in high-dimensional spatially correlated setups such as image
reconstruction.
They are optimized to deal with the typically high number of degrees of freedom.

HMCF adds an HMC sampler to NIFTy that automatically adjusts the many free
parameters steering the HMC sampling machinery. A wide variety of features
ensure efficient full-posterior sampling for high-dimensional inference
problems.
These features include integration step size adjustment, evaluation of the mass
matrix, convergence diagnostics, higher order symplectic integration and 
simultaneous sampling of parameters and hyperparameters in Bayesian hierarchical
models.


Installation
------------

### Requirements

- [Python](https://www.python.org/) (>=3.5)
- [SciPy](https://www.scipy.org/)
- [h5py](https://www.h5py.org/)
- [nifty4](https://gitlab.mpcdf.mpg.de/ift/NIFTy/tree/NIFTy_4)


### Installation

In the following, we assume a Unix-like system with Python3>=3.5.1 and pip>=9.0.1 already installed.
NIFTy4 also requires the libfftw3-dev library.
HMCF and all its required dependencies (including NIFTy) can be installed via pip:

    pip install git+https://gitlab.mpcdf.mpg.de/ift/HMCF

HMCF demos rely additionally on matplotlib

    pip install matplotlib

and for using HMC tools PyQt5 is mandatory, too:

    pip install pyqt5
    
    
### First Steps

For a quick start dive into HMCF by running one of the demonstrations, e.g.:

    python3 demos/wiener_filter.py
       
### Further Documentation

For a detailled description of the whole package, see this [paper](https://arxiv.org/abs/1807.02709).
Note that the exact version of this software described in the paper can be found
on the [jss_publication](https://gitlab.mpcdf.mpg.de/ift/HMCF/tree/jss_publication)
branch. 
    
### Release Notes

The HMCF package is licensed under the terms of the
[GPLv3](https://www.gnu.org/licenses/gpl.html) and is distributed
*without any warranty*.

