# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

from hmcf import load
import numpy as np
import os


__all__ = ['get_autocorrelation']


def get_autocorrelation(path, shift=1, field_name=None):
    """
    Calculates the autocorrelation for the samples of a h5 run-file.
    It follows the convention of numpy correlate which is
    auto_corr[shift] = sum_n samples[n+shift] * conj(samples[n])

    Parameters
    ----------
    path: str
       path to run file or sampler directory.
       If the latter is used, the latest run file in this directory is evaluated.
    shift : int
    field_name : str, optional
        If MultiFields were used, this specifies which of the sub-fields is supposed to be used.
    Returns
    -------
    auto_corr : np.ndarray
        1 + n dimensions, where the first dimensions represents the chains,
        the other n dimensions the field values
    """
    samples = load(path=path)
    if isinstance(samples, dict):
        if field_name is None:
            raise ValueError("Samples are present as MultiFields. This requires the field_name keyword argument. ")
        samples = samples[field_name]
    samples[np.isnan(samples)] = 0
    shift_samples = samples.copy()
    reverse = shift < 0
    shift = abs(shift)
    shift_samples[:, :-shift] = shift_samples[:, shift:]
    shift_samples[:, -shift:] = 0
    if reverse:
        auto_corr = np.sum(samples * np.conjugate(shift_samples),  axis=1)
    else:
        auto_corr = np.sum(shift_samples * np.conjugate(samples), axis=1)
    return auto_corr
