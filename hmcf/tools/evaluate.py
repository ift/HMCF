# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

from .matplotlib_gui import EvaluationWindow
import numpy as np
import nifty4 as ift
from hmcf import load, load_mean
import sys
from PyQt5 import QtWidgets


__all__ = ['show_trajectories']


def show_trajectories(path, reference_field=None, field_name=None, solution=None, start=0, stop=None, step=1):
    """
    A GUI for examining the trajectories of the individual Markov chains.

    Parameters
    ----------
    path : str
        path to the h5 file containing the samples or the directory where the run files are located.
        If a directory is passed, the latest run file is loaded.
    reference_field : ift.Field, ift.MultiField, np.ndarray, dict(str -> np.ndarray)
        the image of interest. As string the following statements are valid:
            'mean' : use the final mean value of the sampling process. If there are only burn_in samples use the mean
                     of the last sample of each chain
        it is also possible to pass your own reference image as either ift-field or numpy array. Check shape and
        dimensions though
    field_name : str, optional
        If MultiFields were used, this specifies which of the sub-fields is supposed to be displayed.
    solution : ift.Field, ift.MultiField, np.ndarray, optional
        If known the 'right' solution for your sampling problem
    start : int
        Use samples starting with start.
        Default: 0
    stop : int, optional
        Use samples until stop.
    step : int
        Only use every 'step' sample.
        Default: 1
    """
    # load required samples
    burn_in_samples = load(path, 'burn_in', start=start, stop=stop, step=step)
    if isinstance(burn_in_samples, dict):
        if field_name is None:
            raise ValueError("Samples are present as MultiFields. This requires the field_name keyword argument. ")
        burn_in_samples = burn_in_samples[field_name]
    burn_in_length = burn_in_samples.shape[1]
    if stop is None:
        samples_stop = None
        load_samples = True
    else:
        samples_stop = stop - (burn_in_length + start)
        load_samples = samples_stop > 0
    samples_start = 0 if start < burn_in_length else start-burn_in_length
    if load_samples:
        try:
            samples = load(path, 'samples', start=samples_start, stop=samples_stop, step=step)
            if isinstance(samples, dict):
                samples = samples[field_name]
        except KeyError:
            all_samples = burn_in_samples
        else:
            all_samples = np.concatenate([burn_in_samples, samples], axis=1)
    else:
        all_samples = burn_in_samples

    # check reference_field input and set field_val accordingly
    if reference_field is None:
        try:
            reference_field = load_mean(path)
        except (KeyError, ValueError):
            reference_field = np.nanmean(burn_in_samples, axis=(0, 1))
    if isinstance(reference_field, dict):
        reference_field = reference_field[field_name]
    if isinstance(reference_field, ift.Field):
        reference_field = reference_field.val

    if solution is not None:
        if isinstance(solution, ift.MultiField) or isinstance(solution, dict):
            solution = solution[field_name]
        if isinstance(solution, ift.Field):
            solution = solution.val

    if burn_in_length <= 0:
        burn_in_length = None

    # start QT App
    q_app = QtWidgets.QApplication(sys.argv)
    aw = EvaluationWindow(reference_field, all_samples, solution=solution, max_burn_in_ix=burn_in_length)
    aw.setWindowTitle('HMC Sampling Trajectories')
    aw.show()
    sys.exit(q_app.exec_())
