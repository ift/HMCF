# embedding_in_qt5.py --- Simple Qt5 application embedding matplotlib canvases
#
# Copyright (C) 2005 Florent Rougon
#               2006 Darren Dale
#               2015 Jens H Nielsen
#
# This file is an example program for matplotlib. It may be used and
# modified with no restriction; raw copies as well as modified versions
# may be distributed without limitation.
#
# The original example has been modified by Christoph Lienhard in 2018


from __future__ import unicode_literals
import numpy as np
import matplotlib
# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from PyQt5 import QtCore, QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT
from matplotlib.figure import Figure


__all__ = ['EvaluationWindow']


class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.compute_initial_figure()

    def compute_initial_figure(self):
        pass


class FieldCanvas1D(MyMplCanvas):

    def __init__(self, *args, **kwargs):
        super(FieldCanvas1D, self).__init__(*args, **kwargs)

    def compute_initial_figure(self):
        self.axes.clear()
        self.axes.plot(np.zeros((50,)))

    def update_figure(self, field_vals):
        self.axes.clear()
        self.axes.plot(field_vals)


class FieldCanvas2D(MyMplCanvas):

    def __init__(self, *args, **kwargs):
        super(FieldCanvas2D, self).__init__(*args, **kwargs)

    def compute_initial_figure(self):
        self.axes.clear()
        self.axes.imshow(np.zeros((50, 50)), interpolation='none')

    def update_figure(self, field_vals):
        self.axes.clear()
        cax = self.axes.imshow(field_vals, interpolation='none')
        self.figure.colorbar(cax, orientation='horizontal')


class TrajectoriesCanvas(MyMplCanvas):

    def __init__(self, *args, **kwargs):
        self._max_burn_in_ix = kwargs.pop("max_burn_in_ix", None)
        super(TrajectoriesCanvas, self).__init__(*args, **kwargs)

    def compute_initial_figure(self):
        self.axes.plot([0, 1], [0, 0], 'r')

    def update_figure(self, trajectories, real_val=None):
        legend = []
        x_max = trajectories.shape[1]
        self.axes.cla()
        if real_val is not None:
            if not np.isnan(real_val):
                legend.append('real')
                self.axes.plot([0, x_max], [real_val, real_val])
        for index, trajectory in enumerate(trajectories):
            legend.append('chain ' + str(index))
            self.axes.plot(trajectory, '-')
        if self._max_burn_in_ix is not None:
            self.axes.axvline(self._max_burn_in_ix, ls='--', c='red')
            legend.append('end of burn-in')
        self.axes.legend(legend)
        self.draw()


class EvaluationWindow(QtWidgets.QMainWindow):
    def __init__(self, field, samples, solution=None, max_burn_in_ix=None):
        QtWidgets.QMainWindow.__init__(self)
        self.samples = samples
        if solution is None:
            solution = np.full(self.samples.shape[2:], np.nan)
        self.solution = solution
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("application main window")

        self.main_widget = QtWidgets.QWidget(self)

        pxl = QtWidgets.QLabel('Pixel: ', self)
        self.pxl1_edit = QtWidgets.QLineEdit('0')
        self.pxl2_edit = QtWidgets.QLineEdit('0')

        btn = QtWidgets.QPushButton('Show', self)
        btn.clicked.connect(self.show_new)
        btn.move(100, 10)
        hbox1 = QtWidgets.QHBoxLayout()
        hbox1.addWidget(pxl)
        hbox1.addWidget(self.pxl1_edit)
        hbox1.addWidget(self.pxl2_edit)
        hbox1.addWidget(btn)

        if len(field.shape) == 1:
            self.bpd = FieldCanvas1D(self.main_widget, width=10, height=10, dpi=100)
        elif len(field.shape) == 2:
            self.bpd = FieldCanvas2D(self.main_widget, width=10, height=10, dpi=100)
        else:
            raise ValueError('Cannot display '+str(len(field.shape))+' dimensional field')
        self.trs = TrajectoriesCanvas(self.main_widget, width=10, height=10, dpi=100, max_burn_in_ix=max_burn_in_ix)
        self.bpd.mpl_connect('button_press_event', self.onclick)
        hbox2 = QtWidgets.QHBoxLayout()
        hbox2.addWidget(self.bpd)
        hbox2.addWidget(self.trs)
        self.addToolBar(QtCore.Qt.BottomToolBarArea, NavigationToolbar2QT(self.trs, self))

        l = QtWidgets.QVBoxLayout(self.main_widget)
        l.addLayout(hbox1)
        l.addLayout(hbox2)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.bpd.update_figure(field)
        self.show_new()

        self.statusBar().showMessage("All hail matplotlib!", 2000)

    def show_new(self):
        x = int(self.pxl1_edit.text())
        y = int(self.pxl2_edit.text())
        if len(self.solution.shape) == 1:
            self.trs.update_figure(self.samples[:, :, x], self.solution[x])
        else:
            self.trs.update_figure(self.samples[:, :, y, x], self.solution[y, x])

    def file_quit(self):
        self.close()

    def closeEvent(self, ce):
        self.file_quit()

    def onclick(self, event):
        try:
            x, y = (int(np.round(event.xdata)), int(np.round(event.ydata)))
        except (AttributeError, IndexError):
            # probably just click outside of image and therefore event.xdata is not defined
            pass
        else:
            self.pxl1_edit.setText(str(x))
            self.pxl2_edit.setText(str(y))
            self.show_new()
