# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

"""
this module provides some functionality to probe a mass matrix given the curvature of the potential.
"""
import numpy as np
import nifty4 as ift
from ..logging import logger


__all__ = ['get_mass_operator', 'MassBase', 'MassMain', 'MassChain']


def get_mass_operator(curvature, probe_count=10, dtype=None):
    """
    probes the curvature of the potential of a HMC hamiltonian to generate an appropriate mass matrix (using the inverse
    of the probed diagonal at the position of the potential).

    Parameters
    ----------
    curvature : ift.EndomorphicOperator
        curvature of potential of HMC Hamiltonian
    probe_count : int
        number of probing counts
    dtype : type, optional

    Returns
    -------
    mass : ift.EndomorphicOperator
    """
    if probe_count < 2:
        raise RuntimeError
    # sample_sum = ift.Field.zeros(curvature.domain, dtype=dtype)
    sample = curvature.draw_sample(from_inverse=True, dtype=dtype)
    sample_squared_sum = sample.conjugate() * sample

    for i in range(probe_count - 1):
        sample = curvature.draw_sample(from_inverse=True, dtype=dtype)
        sample_squared_sum += sample.conjugate() * sample
    inv_diagonal = sample_squared_sum / (probe_count-1)
    if (inv_diagonal == 0.).any():
        raise ValueError("Inverse diagonal of mass operator contains zero-entries.")
    return ift.makeOp(1. / inv_diagonal)


class MassBase(object):
    """
    class handling the mass operator during HMC sampling. Base class for the use case of each chain handling its own
    mass.

    The mass is reevaluated if the chains have converged (wrt their current convergence level) and the attribute
    mass_reevaluations is bigger than 0.

    Attributes
    ----------
    shared : bool
        whether or not chains share the same mass matrix (and reevaluate mass matrix based on 'shared properties')
        Default: False
    num_samples : int
        number of samples used during mass matrix reevaluation

    Raises
    ------
    ValueError
        if during mass reevaluation a non-positive definite matrix appears

    See Also
    --------
    MassHandler
        the counterpart for the main process during sampling
    """
    def __init__(self, potential, num_chains=1, num_samples=50, mass_reevaluations=1):
        """
        Parameters
        ----------
        potential : ift.Energy
            hmc potential
        num_chains : int
            number of chains (this is basically a dub for inherited classes like MassHandler)
        num_samples : int
            number of samples used for the covariance based calculation of the mass matrix
        mass_reevaluations : int
            number of times the mass matrix is supposed to be reevaluated
        """
        self._potential = potential
        self._domain = self._potential.position.domain
        self._dtype = self._potential.position.dtype
        self._num_chains = num_chains
        self.num_samples = num_samples

        self.shared = False
        self._get_initial_mass = True
        self._operator = self._get_initial_operator()
        self._reevaluations = np.full(self._num_chains, mass_reevaluations, dtype=int)

    @property
    def get_initial_mass(self):
        """bool : Whether or not evaluating a mass in the beginning without waiting for convergence of the chains.
        Setting it to True/False increases/decreases reevaluations by 1
        Default: True"""
        return self._get_initial_mass

    @get_initial_mass.setter
    def get_initial_mass(self, value):
        if value:
            self._reevaluations += 1
        else:
            self._dec_reevaluations()
        self._get_initial_mass = value

    @property
    def reevaluations(self):
        """np.ndarray of int: number of mass reevaluations taken out during burn in phase in each chain.
        Setting with a simple int sets all chains to the same value.
        Default: np.ones(num_chains)"""
        return self._reevaluations

    @reevaluations.setter
    def reevaluations(self, value):
        if not hasattr(value, '__iter__'):
            self._reevaluations = np.full(self._num_chains, value, dtype=int)
            if value == 0:
                self._get_initial_mass = False
        else:
            self._reevaluations = np.array(value, dtype=int)
            if all(val == 0 for val in value):
                self._get_initial_mass = False

    @property
    def operator(self):
        """ift.EndomorphicOperator : The mass operator.
        If get_initial_mass is True, setting operator will change get_initial_mass to False
        and decrease reevaluations by 1"""
        return self._operator

    @operator.setter
    def operator(self, value):
        if self._get_initial_mass:
            self._get_initial_mass = False
            self._dec_reevaluations()
        self._operator = value

    def _get_initial_operator(self):
        return ift.ScalingOperator(1., domain=self._domain)

    def _dec_reevaluations(self):
        mask = self._reevaluations > 0
        self._reevaluations[mask] -= 1

    def _get_mass_operator(self, curvature):
        return get_mass_operator(curvature, probe_count=self.num_samples, dtype=self._dtype)

    def update(self, incoming, outgoing):
        raise NotImplementedError

    def reset(self, reset_operator=False):
        if reset_operator:
            self._operator = self._get_initial_operator()


class MassMain(MassBase):
    """
    Main process counterpart to MassChain class.
    For non-shared mass operators this class mostly handles information related tasks.
    For shared mass operations it also reevaluates mass matrices.
    """
    def __init__(self, potential, num_chains=1, num_samples=50, mass_reevaluations=1):
        """
        Parameters
        ----------
        potential : ift.Energy
            hmc potential
        num_chains : int
            number of chains (this is basically a dub for inherited classes like MassHandler)
        num_samples : int
            number of samples used for the covariance based calculation of the mass matrix
        mass_reevaluations : int
            number of times the mass matrix is supposed to be reevaluated
        """
        super(MassMain, self).__init__(potential=potential, num_chains=num_chains, num_samples=num_samples,
                                       mass_reevaluations=mass_reevaluations)

        self._new_mass_flag = np.full(self._num_chains, False, dtype=bool)
        self._current_positions = [self._potential.position.copy() for _ in range(self._num_chains)]
        self._got_current_position = self._new_mass_flag.copy()
        get_init_mass_val = self._get_initial_mass
        self._get_initial_mass = np.full(self._num_chains, get_init_mass_val)

    @MassBase.get_initial_mass.getter
    def get_initial_mass(self):
        return self._get_initial_mass.any()

    @get_initial_mass.setter
    def get_initial_mass(self, value):
        if value:
            self._reevaluations += 1
        else:
            self._dec_reevaluations()
        self._get_initial_mass = np.full(self._num_chains, value)

    def get_client(self):
        """
        get corresponding mass instance for subprocesses

        Returns
        -------
        mass_client : MassChain
        """
        mass_client = MassChain(self._potential, num_samples=self.num_samples,
                                mass_reevaluations=self._reevaluations[0])
        mass_client._operator = self._operator
        mass_client._get_initial_mass = self._get_initial_mass.any()
        mass_client.shared = self.shared
        return mass_client

    def update(self, incoming, outgoing):
        """
        appends sample to its history and dependent on class flags tries to get a new (better) mass matrix if possible.

        Notes
        -----
        adds stuff to the incoming dictionary (mostly log messages)
        adds stuff to the outgoing dictionary (e.g. new mass operator if shared and reevaluated)

        Parameters
        ----------
        incoming : dict
            the package packed by each chain for sending stuff to the main process. It should contain 'sample',
            'accepted', and 'epsilon'
        outgoing :  dict
            the package packed by the main process for sending stuff to the chain. Should contain 'converged'.
        """
        ch_id = incoming['chain_identifier']
        step = incoming['step']
        new_mass_flag = False
        if self.shared:
            engage = incoming['accepted']
            nominal_clearance = outgoing['converged'] and self._reevaluations[ch_id] > 0
            if self._new_mass_flag[ch_id]:  # check if there is an new 'shared' mass already
                self._new_mass_flag[ch_id] = False
                new_mass_flag = True

            elif engage and (self._get_initial_mass[ch_id] or nominal_clearance):
                # add a new sample
                self._current_positions[ch_id] = incoming['sample']
                self._got_current_position[ch_id] = True
                # try curvature based approach (every time the conditions above are met)
                position = self._get_position_for_reeval()
                curvature = self._potential.at(position).curvature

                try:
                    new_op = self._get_mass_operator(curvature)
                except ValueError:
                    logger.warning('Curvature not positive definite, try again in next step. (step {:d})'.format(step))
                    new_mass_flag = False
                else:
                    logger.info('Mass matrix reevaluated. (step {:d})'.format(step))
                    new_mass_flag = True
                    self._operator = new_op
                    self.reset(reset_operator=False)
                    self._new_mass_flag = np.full_like(self._new_mass_flag, True)
                    self._new_mass_flag[ch_id] = False

            # if this was an initial mass thing, remember to start the real work.
            if new_mass_flag:
                outgoing.update(new_mass=self._operator)
                if self._get_initial_mass[ch_id]:
                    self._get_initial_mass = np.full(self._num_chains, False)
                else:
                    self._reevaluations[ch_id] -= 1
        else:
            # chains do all the work. Just do some stuff for information purposes
            if incoming['mass_reeval']:
                self._reevaluations[ch_id] -= 1
                if self._get_initial_mass[ch_id]:  # this is problematic since it should be an num_chains dim vector.
                    self._get_initial_mass[ch_id] = False
                new_mass_flag = True
        return new_mass_flag

    def _get_position_for_reeval(self):
        position_sum = self._current_positions[0].copy()
        for position in self._current_positions[1:]:
            position_sum += position
        return position_sum / len(self._current_positions)

    def reset(self, reset_operator=False):
        super(MassMain, self).reset(reset_operator=reset_operator)
        self._current_positions = [self._potential.position.copy() for _ in range(self._num_chains)]
        self._got_current_position = self._new_mass_flag.copy()


class MassChain(MassBase):
    """
    Chain process counterpart to MassMain class.
    For non-shared mass operators this class mostly just waits for a new mass matrix.
    For non-shared mass operations it reevaluates mass matrices itself.
    """
    def __init__(self, potential, num_samples=50, mass_reevaluations=1):
        """
        Parameters
        ----------
        potential : ift.Energy
            hmc potential
        num_samples : int
            number of samples used for the covariance based calculation of the mass matrix
        mass_reevaluations : int
            number of times the mass matrix is supposed to be reevaluated
        """
        super(MassChain, self).__init__(potential=potential, num_chains=1, num_samples=num_samples,
                                        mass_reevaluations=mass_reevaluations)

    def update(self, incoming, outgoing):
        """
        appends sample to its history and dependent on class flags tries to get a new (better) mass matrix if possible.

        Notes
        -----
        Adds stuff to outgoing dictionary!

        Parameters
        ----------
        incoming : dict
            the package packed by the main process for sending stuff to the chain. Should contain 'converged'.
        outgoing : dict
            the package packed by each chain for sending stuff to the main process. It should contain 'sample',
            'accepted', and 'epsilon'

        Returns
        -------
        set_new_mass : bool
            whether or not the mass operator has been successfully reevaluated.
        """
        ch_id = 0
        new_mass_flag = False
        step = outgoing['step']
        outgoing.update(dec_convergence_level=False)
        outgoing.update(mass_reeval=False)
        if self.shared:
            if 'new_mass' in incoming:
                self._operator = incoming['new_mass']
                new_mass_flag = True
                logger.info('Received new Mass matrix. (step {:d})'.format(step))
        else:
            sample = outgoing['sample']  # type: ift.Field
            engage = outgoing['accepted']
            nominal_clearance = incoming['converged'] and self._reevaluations[ch_id] > 0
            if engage and (self._get_initial_mass or nominal_clearance):

                # curvature based approach
                curvature = self._potential.at(sample).curvature
                try:
                    new_op = self._get_mass_operator(curvature)
                except ValueError:
                    logger.warning('Curvature not positive definite, try again in next step. (step {:d})'.format(step))
                    new_mass_flag = False
                else:
                    logger.info('Mass matrix reevaluated. (step {:d})'.format(step))
                    new_mass_flag = True
                    self._operator = new_op
                    self.reset(reset_operator=False)

        if new_mass_flag:
            outgoing.update(mass_reeval=True)
            if self._get_initial_mass:
                self._get_initial_mass = False
            else:
                self._reevaluations[ch_id] -= 1
                outgoing.update(dec_convergence_level=True)
        return new_mass_flag
