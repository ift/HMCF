# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

from time import time
import numpy as np
import curses
from ..convergence import Convergence
from .chain import Epsilon
from ..logging import logger, DisplayHandler
from logging import INFO, DEBUG, StreamHandler, Formatter, NullHandler


__all__ = ['Display', 'LineByLineDisplay', 'TableDisplay']


class Display(object):
    """Base class for possibly more than one class defining the appearance of the large number
    of available information during sampling.
    This class can be used as an 'information display' but it shows nothing and leaves (other) terminal output
    unchanged.
    """
    _logging_hdlr = NullHandler()

    def __init__(self, num_chains=1):
        self._num_chains = num_chains

        # logging related stuff
        self._logging_hdlr.setLevel(logger.level)

    def init_display(self):
        pass

    def update_chain(self, convergence, package, num_samples, acceptance_rate):
        """
        Parameters
        ----------
        convergence : Convergence
        package : dict
        num_samples : int
            total number of steps after burn_in per chain
        acceptance_rate : float
            acceptance rate of samples drawn after burn_in
        """
        pass

    def reset_display(self):
        pass

    def close(self):
        pass

    def setLevel(self, level):
        """Sets the logging level during HMC sampling for the display."""
        self._logging_hdlr.setLevel(level)

    def _update_num_chains(self, new_val):
        """mostly for inheritance"""
        self._num_chains = new_val


class LineByLineDisplay(Display):
    """prints information about chains line by line. Output changes slightly if attribute 'level' is set differently"""
    burn_in_info_line = """Accepted: {!r:5}, Converged: {!r:5}, ConvLevel: {:3d}"""
    burn_in_debug_line = """dEnergy: {:9.2E}, ConvMeas: {:8.2E}, Epsilon: {:8.2E}"""
    sampling_info_line = """Sampling progress: {:6.2f}%, Acceptance Rate: {:4^.2f}"""

    _logging_hdlr = DisplayHandler(100)
    _logging_hdlr_fmt = Formatter('{asctime} - {levelname}: {message}', style='{', datefmt='%H:%M:%S')

    def __init__(self, num_chains=1):
        super(LineByLineDisplay, self).__init__(num_chains=num_chains)
        self._old_stream_hdlrs = []
        for hdlr in logger.handlers:
            if isinstance(hdlr, StreamHandler):
                self._old_stream_hdlrs.append(hdlr)
                logger.removeHandler(hdlr)
        logger.addHandler(self._logging_hdlr)

    def update_chain(self, convergence, package, num_samples, acceptance_rate):
        """
        Parameters
        ----------
        convergence : Convergence
        package : dict
        num_samples : int
            total number of steps after burn_in per chain
        acceptance_rate : float
            acceptance rate of samples drawn after burn_in
        """
        ch_id = package['chain_identifier']
        msg = ''
        if package['burn_in']:
            if self._logging_hdlr.level <= INFO:
                msg += (self.burn_in_info_line.format(package['accepted'], convergence.converged[ch_id],
                                                      convergence.level[ch_id]) + '\n')
            if self._logging_hdlr.level <= DEBUG:
                msg += (self.burn_in_debug_line.format(package['delta_energy'], convergence.measure_max[ch_id],
                                                       package['epsilon'].val) + '\n')
        else:
            if self._logging_hdlr.level <= INFO:
                progress = float(package['step']) / num_samples
                msg += self.sampling_info_line.format(progress*100., acceptance_rate) + '\n'

        if msg:
            print('chain', ch_id)
            print(msg)
            print('-'*80)
        self._print_logs()

    def _print_logs(self):
        for msg in self._logging_hdlr.buffer:
            print(msg)
            print('-'*80)
        self._logging_hdlr.flush()

    def close(self):
        logger.removeHandler(self._logging_hdlr)
        for hdlr in self._old_stream_hdlrs:
            logger.addHandler(hdlr)
        self._old_stream_hdlrs = []


class TableDisplay(Display):
    """Class for displaying information during sampling in a nice way.

    Creates a terminal table containing all relevant information for each chain """

    header1 = """ ch. | acc. |  dEnergy  | Convergence                | Epsilon                 """
    header2 = """     | rate |           | conv. lev meas.    quota   | value    conv. meas.    """
    break_line = """=====+======+===========+============================+========================="""
    row_burn_in = """ {:3d} | {:4^.2f} | {:9.2E} | {!r:5} {:3d} {:8.2E} {:6.2f}% | {:8.2E} {!r:5} {:8.2E} """
    progress_bar_length = 33
    row_sampling = """ {:3d} | {:4^.2f} | {:9.2E} | Progress:  {:""" + str(progress_bar_length) + """}  {:6.2f}% """

    _logging_hdlr = DisplayHandler(2000)
    _logging_hdlr_fmt = Formatter('{asctime} - {levelname}: {message}', style='{', datefmt='%H:%M:%S')

    def __init__(self, num_chains=1, title=''):
        """
        Parameters
        ----------
        num_chains : int
            number of chains
        title : str, optional
            Title for table
        """
        super(TableDisplay, self).__init__(num_chains=num_chains)
        self._accepted_hist = np.full((self._num_chains, 20), False, dtype=bool)
        self._accepted_index = np.zeros(self._num_chains, dtype=int)
        self._draw_all_T = 15.  # seconds after which full table gets drawn
        self._draw_all_last_time = time()
        self._window = None  # set this in init_display properly
        self._window_shape = (0, 0)

        # init table
        self._table = []
        self._first_chain_index = 3
        self._eot = len(self._table)-1  # end of table index
        self._title = title
        self._reset_table()

        # for logging
        self._old_stream_hdlrs = []
        self._logging_hdlr.setFormatter(self._logging_hdlr_fmt)

    def _reset_table(self):
        if self._title == '':
            self._table = []
            self._first_chain_index = 3
        else:
            self._table = [('Title: '+self._title)[:80], ' ']
            self._first_chain_index = 5
        self._table += [self.header1, self.header2, self.break_line]
        self._table += [self._init_row(chain_identifier=i) for i in range(self._num_chains)]
        self._table += ['']  # add an extra line at the end for possible error statements (from e.g. numpy)
        self._eot = len(self._table)-1  # end of table index

    def _update_num_chains(self, new_val):
        super(TableDisplay, self)._update_num_chains(new_val=new_val)
        self._reset_table()

    def init_display(self):
        for hdlr in logger.handlers:
            if isinstance(hdlr, StreamHandler):
                self._old_stream_hdlrs.append(hdlr)
                logger.removeHandler(hdlr)
        logger.addHandler(self._logging_hdlr)

        self._window = curses.initscr()
        self._window.clear()
        self._window.leaveok(0)
        curses.noecho()
        curses.cbreak()
        self._window.clear()
        self._window_shape = self._window.getmaxyx()
        for i, line in enumerate(self._table):
            self._draw(i, line)
        self._eot = len(self._table)-1
        self._draw_log_msgs()
        curses.curs_set(2)

    def _draw_log_msgs(self):
        lines_to_end = self._window_shape[0] - self._eot - 1
        for i, msg in enumerate(self._logging_hdlr.buffer[-lines_to_end:]):
            self._draw(self._eot+1+i, msg[:80])

    def update_chain(self, convergence, package, num_samples, acceptance_rate):
        """
        Parameters
        ----------
        convergence : Convergence
        package : dict
        num_samples : int
            total number of steps after burn_in per chain
        acceptance_rate : float
            acceptance rate of samples drawn after burn_in
        """
        ch_id = package['chain_identifier']
        epsilon = package['epsilon']  # type: Epsilon
        accepted = package['accepted']
        delta_energy = package['delta_energy']
        num_mass_samples = package.get('num_mass_samples', -1)
        self._accepted_hist[ch_id, self._accepted_index] = accepted
        self._accepted_index[ch_id] = (self._accepted_index[ch_id] + 1) % self._accepted_hist.shape[1]
        if package['burn_in']:
            new_row = self.row_burn_in.format(ch_id, np.mean(self._accepted_hist[ch_id]), delta_energy,
                                              convergence.converged[ch_id], convergence.level[ch_id],
                                              convergence.measure_max[ch_id], convergence.quota[ch_id]*100.,
                                              epsilon.val, epsilon.converged, np.max(epsilon.convergence_measure),
                                              num_mass_samples)
        else:
            progress = float(package['step']) / num_samples
            # fill_string = '█'*int(progress*45) + '-'*(45-int(progress*45))
            fill_string = ('|' * int(progress * self.progress_bar_length) + '-'
                           * (self.progress_bar_length - int(progress * self.progress_bar_length)))
            new_row = self.row_sampling.format(ch_id, acceptance_rate, delta_energy, fill_string, progress*100.)
        self._draw(ch_id+self._first_chain_index, new_row)
        self._table[ch_id+self._first_chain_index] = new_row
        self._draw_log_msgs()

        if time() > self._draw_all_last_time + self._draw_all_T:
            self.init_display()  # actual table length (w/ log msgs) only changes right here
            self._draw_all_last_time = time()

    def _draw(self, y, string):
        try:
            self._window.move(y, 0)
            self._window.clrtoeol()
            self._window.addstr(y, 0, string)
            self._window.move(self._eot+1, 0)  # avoid random prints from somewhere messing up the table
            self._window.refresh()
        except curses.error as e:
            self.reset_display()
            logger.warning('Curses error occurred.', exc_info=e)

    def _init_row(self, chain_identifier=0, accepted=True, delta_energy=np.inf, converged=False, conv_level=0,
                  conv_meas_max=np.inf, epsilon=None, num_mass_samples=0):
        if epsilon is None:
            eps_val = 0.005
            eps_conv = False
            eps_meas = [np.Inf, np.Inf]
        else:
            eps_val = epsilon.val
            eps_conv = epsilon.converged
            eps_meas = epsilon.convergence_measure
        ch_id = chain_identifier
        self._accepted_hist[ch_id, self._accepted_index] = accepted
        self._accepted_index[ch_id] = (self._accepted_index[ch_id] + 1) % len(self._accepted_hist)
        new_row = self.row_burn_in.format(ch_id, np.mean(self._accepted_hist[ch_id]), delta_energy, converged,
                                          conv_level, conv_meas_max, 0., eps_val, eps_conv, np.max(eps_meas),
                                          num_mass_samples)
        return new_row

    def reset_display(self):
        self._reset_table()
        self.init_display()

    def close(self):
        curses.echo()
        curses.nocbreak()
        curses.endwin()
        logger.removeHandler(self._logging_hdlr)
        for hdlr in self._old_stream_hdlrs:
            logger.addHandler(hdlr)
        self._old_stream_hdlrs = []
        # print a snapshot of the last table
        for line in self._table:
            print(line)
        print('')
        for msg in self._logging_hdlr.buffer:
            print(msg)
        self._logging_hdlr.flush()
