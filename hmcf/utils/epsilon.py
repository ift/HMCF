# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

import numpy as np
import warnings
from scipy.optimize import curve_fit, OptimizeWarning
import random


__all__ = ['Epsilon', 'EpsilonConst', 'EpsilonSimple', 'EpsilonPowerLaw', 'EpsilonPowerLawDivergence',
           'EpsilonExponential', 'EpsilonOneOverX']


class Epsilon(object):
    """
    Class to handle adjusting the epsilon parameter in the integration.

    Attributes
    ----------
    val : float
        Value for epsilon.
    limits : list float
        Lower and upper bound for epsilon to avoid unrealistic values, especially in the beginning of optimization
        Default: [1.E-20, 10.]
    convergence_tolerance : float
        Defines the range in which epsilon optimization is said to have converged.
        Default: 0.5
    divergence_threshold : float
        A value for which abs(denergy) is assumed to be divergent.
        Default: 1.E20
    """
    def __init__(self, target_acceptance_rate=0.8, limits=None):
        """
        Parameters
        ----------
        target_acceptance_rate : float
            wanted acceptance rate during sampling
        limits : list of float
            list with two elements containing minimum and maximum value of epsilon (default: [1.E-10, 10]
        """
        self._locality = 50
        self._tar = target_acceptance_rate
        if limits is None:
            self.limits = [1.E-40, 10.]
        else:
            self.limits = limits  # type: list float

        self._eps_hist = np.full(self._locality, np.nan, dtype=float)
        self._denergy_hist = np.full(self._locality, np.nan, dtype=float)
        self._hist_index = 0

        self.val = 0.005

        self._converged = False
        self._convergence_measure = [np.inf, np.inf]
        self._min_samples = 30
        self.convergence_tolerance = 0.5

        self.divergence_threshold = 1E50
        self._divergence_lock_counter = 0  # if denergy is divergent in one step, lock 'converged' for next 10 steps

    @property
    def locality(self):
        """int: how many recent epsilon/dEnergy pairs play a role in everything. Setting this will reset the instance!
        default: 50"""
        return self._locality

    @locality.setter
    def locality(self, value):
        if int(value) > 0:
            self._locality = value
        else:
            self._locality = 1
            print('locality has to be bigger than zero. Set to 1.')
        if self._locality < 30:
            self._min_samples = self._locality
        else:
            self._min_samples = 30
        self.reset()

    @property
    def target_acceptance_rate(self):
        """float : target value of acceptance rate for which epsilon is optimized. 1. should be avoided. Setting this
        will reset the instance."""
        return self._tar

    @target_acceptance_rate.setter
    def target_acceptance_rate(self, value):
        self._tar = value
        self.reset(start_val=self.val)

    @property
    def convergence_measure(self):
        return self._convergence_measure

    @property
    def converged(self):
        return self._converged

    def update(self, new_delta_energy):
        self._append_data(new_delta_energy)
        self._update_convergence_measure()
        self._update_converged()
        if not self.converged:
            # freeze value if converged!
            self._update_val(new_delta_energy)
            self._check_val()

    def _append_data(self, new_delta_energy):
        if np.abs(new_delta_energy) < self.divergence_threshold:
            self._divergence_lock_counter -= 1
            self._eps_hist[self._hist_index] = self.val
            self._denergy_hist[self._hist_index] = np.abs(new_delta_energy)
            self._hist_index = (self._hist_index + 1) % self._locality
        else:
            self._divergence_lock_counter = 10

    def _update_val(self, new_delta_energy):
        raise NotImplementedError

    def _check_val(self):
        if self.val < self.limits[0]:
            self.val = self.limits[0]
        if self.val > self.limits[1]:
            self.val = self.limits[1]

    def _update_convergence_measure(self):
        if np.sum(np.logical_not(np.isnan(self._eps_hist))) > 0:
            # avoid empty array warnings. I hate warnings. Who wants that shit? Make an exception or nothing, idiots...
            try:
                eps_mean = np.nanmean(self._eps_hist)
                if eps_mean != 0.:
                    self._convergence_measure[0] = np.nanvar(self._eps_hist) / np.abs(eps_mean)
                else:
                    self._convergence_measure[0] = np.inf
                self._convergence_measure[1] = np.abs(np.nanmean(self._denergy_hist) / (2 * (1 - self._tar)) - 1.)
            except FloatingPointError:
                self._convergence_measure = [np.inf, np.inf]

    def _update_converged(self):
        if self._min_samples > np.sum(~np.isnan(self._eps_hist)):
            self._converged = False
        elif self._divergence_lock_counter <= 0:
            self._converged = all(conv_meas < self.convergence_tolerance for conv_meas in
                                  self._convergence_measure)
        else:
            self._converged = False

    def reset(self, start_val=None):
        if start_val is None:
            start_val = 0.005

        self._eps_hist = np.full(self._locality, np.nan, dtype=float)
        self._denergy_hist = np.full(self._locality, np.nan, dtype=float)
        self._hist_index = 0

        self.val = start_val

        self._converged = False
        self._convergence_measure = [np.inf, np.inf]
        self._divergence_lock_counter = 0  # if denergy is divergent in one step, lock 'converged' for next 10 steps


class EpsilonConst(Epsilon):
    """Keeps epsilon parameter constant"""

    def __init__(self, target_acceptance_rate=0.8, limits=None):
        super(EpsilonConst, self).__init__(target_acceptance_rate=target_acceptance_rate, limits=limits)
        self.val = 0.005

    def _update_val(self, new_delta_energy):
        pass

    def _update_convergence_measure(self):
        self._convergence_measure = [0., 0.]

    def _update_converged(self):
        self._converged = True


class EpsilonSimple(Epsilon):
    """
        class to handle adjusting the epsilon parameter in the integration.
        Epsilon gets adjusted by decreasing/increasing it if dEnergy is bigger/smaller than
        2 * (1 - target_acceptance_rate) (look somewhere else, why this is an appropriate choice).

        Attributes
        ----------
        change_range : float
            determines the maximum value for deps in new_epsilon = (1 +- deps) * old_epsilon
            for reasons there is a random factor on deps.
        """
    def __init__(self, target_acceptance_rate=0.8, limits=None):
        """
        Parameters
        ----------
        target_acceptance_rate : float
            wanted acceptance rate during sampling
        limits : list of float
            list with two elements containing minimum and maximum value of epsilon (default: [1.E-10, 10]
        """
        super(EpsilonSimple, self).__init__(target_acceptance_rate=target_acceptance_rate,
                                            limits=limits)
        self.change_range = 0.1

    def _update_val(self, new_delta_energy):
        target_denergy = 2 * (1 - self.target_acceptance_rate)
        new_delta_energy = np.abs(new_delta_energy)
        if new_delta_energy == 0.:
            # epsilon waaaay too small, make it much bigger
            new_val = self.val * (1. + 19. * random.random())
        elif new_delta_energy > self.divergence_threshold:
            # epsilon waaaay too big, make it much smaller
            new_val = self.val * (0.05 + 0.95 * random.random())
        elif new_delta_energy > target_denergy:
            new_val = self.val * ((1. - self.change_range) + self.change_range * random.random())
        elif new_delta_energy <= target_denergy:
            new_val = self.val * (1. + self.change_range * random.random())
        else:
            # this should never occure (possible reasons: new_delta_energy is nan or None), just make it smaller.
            # maybe it helps
            new_val = self.val * (0.05 + 0.95 * random.random())
        self.val = new_val


class EpsilonPowerLaw(Epsilon):
    """
    class to handle adjusting the epsilon parameter in the integration.
    Epsilon gets adjusted by applying the law
    eps_new = eps_old * (1-sgn(dEnergy-target_dEnergy)*abs((dEnergy-target_dEnergy)/(dEnergy + target_dEnergy))**power)
    where target_dEnergy is 2 * (1 - target_acceptance_rate) (look somewhere else, why this is an appropriate choice).

    randomness is necessary to avoid loop like behavior.

    Attributes
    ----------
    power : int
        the power in power law
    """
    def __init__(self, target_acceptance_rate=0.8, limits=None):
        """
        Parameters
        ----------
        target_acceptance_rate : float
            wanted acceptance rate during sampling
        limits : list of float
            list with two elements containing minimum and maximum value of epsilon (default: [1.E-10, 10]
        """
        super(EpsilonPowerLaw, self).__init__(target_acceptance_rate=target_acceptance_rate,
                                              limits=limits)
        self._log_denergy_hist = self._denergy_hist.copy()
        self._log_eps_hist = self._eps_hist.copy()
        self.power = 5
        self._a = 1.
        self._b = 2.

    def _update_val(self, new_delta_energy):
        target_denergy = 2 * (1 - self.target_acceptance_rate)
        new_delta_energy = np.abs(new_delta_energy)
        if new_delta_energy > self.divergence_threshold:
            # epsilon waaaay too big, make it much smaller
            new_val = self.val * (0.05 + 0.95 * random.random())
        else:
            new_val = self.val * (1 - np.sign(new_delta_energy - target_denergy) *
                                  np.abs((new_delta_energy - target_denergy) /
                                         (new_delta_energy + target_denergy))**self.power * random.random())
        self.val = new_val


class EpsilonPowerLawDivergence(Epsilon):
    """
    class to handle adjusting the epsilon parameter in the integration.
    Epsilon gets adjusted by applying the law
    eps_new = eps_old * (1-sgn(dEnergy-target_dEnergy)*abs((dEnergy-target_dEnergy)/(dEnergy + target_dEnergy))**power)
    where target_dEnergy is 2 * (1 - target_acceptance_rate) (look somewhere else, why this is an appropriate choice).

    randomness is not necessary since loop like behavior not possible.

    This is an updated version von EpsilonPowerLaw where divergent behavior leads to a fine tuning of epsilon


    Attributes
    ----------
    power : int
        The power in power law.
        Default: 2
    """
    def __init__(self, target_acceptance_rate=0.8, limits=None):
        """
        Parameters
        ----------
        target_acceptance_rate : float
            wanted acceptance rate during sampling
        limits : list of float
            list with two elements containing minimum and maximum value of epsilon (default: [1.E-10, 10]
        """
        super(EpsilonPowerLawDivergence, self).__init__(target_acceptance_rate=target_acceptance_rate,
                                                        limits=limits)
        self._log_denergy_hist = self._denergy_hist.copy()
        self._log_eps_hist = self._eps_hist.copy()
        self._divergence_counter = 0
        self._diverged_last_time = False
        self.power = 2
        self._a = 1.
        self._b = 2.

    def _update_val(self, new_delta_energy):
        target_denergy = 2 * (1 - self.target_acceptance_rate)
        new_delta_energy = np.abs(new_delta_energy)
        if new_delta_energy > self.divergence_threshold:
            # epsilon waaaay too big, make it much smaller
            new_val = self.val * 0.5
            if not self._diverged_last_time:
                self._divergence_counter += 1
            self._diverged_last_time = True
        else:
            new_val = self.val * (1 - np.sign(new_delta_energy - target_denergy) * 2**(-self._divergence_counter) *
                                  np.abs((new_delta_energy - target_denergy) /
                                         (new_delta_energy + target_denergy))**self.power)
            self._diverged_last_time = False
        self.val = new_val

    def reset(self, start_val=None):
        super(EpsilonPowerLawDivergence, self).reset(start_val=start_val)
        self._divergence_counter = 0
        self._diverged_last_time = False


class EpsilonExponential(Epsilon):
    """
    class to handle adjusting the epsilon parameter in the integration.
    Epsilon gets adjusted by assuming a power law between delta_energy and
    epsilon: log(abs(delta_energy))= a + b*log(epsilon) and then fitting a,b
    to the 'measurement points' calculated during the burn in phase.
    """
    def __init__(self, target_acceptance_rate=0.8, limits=None):
        """
        Parameters
        ----------
        target_acceptance_rate : float
            wanted acceptance rate during sampling
        limits : list of float
            list with two elements containing minimum and maximum value of epsilon (default: [1.E-10, 10]
        """
        super(EpsilonExponential, self).__init__(target_acceptance_rate=target_acceptance_rate,
                                                 limits=limits)
        self._log_denergy_hist = self._denergy_hist.copy()
        self._log_eps_hist = self._eps_hist.copy()
        self._a = 1.
        self._b = 2.

    @property
    def a(self):
        """float: 'a' parameter in log(|delta_energy|)= a + b*log(epsilon)"""
        return self._a

    @property
    def b(self):
        """float: 'b' parameter in log(|delta_energy|)= a + b*log(epsilon)"""
        return self._b

    def _append_data(self, new_delta_energy):
        if np.abs(new_delta_energy) < self.divergence_threshold:
            self._log_eps_hist[self._hist_index] = np.log(self.val)
            self._log_denergy_hist[self._hist_index] = np.log(np.abs(new_delta_energy))
        super(EpsilonExponential, self)._append_data(new_delta_energy=new_delta_energy)

    def _update_val(self, new_delta_energy):
        target_denergy = 2 * (1 - self._tar)
        if new_delta_energy == 0.:
            # epsilon way too small => make it much larger. Add randomness to tackle problems with divergent behavior
            new_val = self.val * (1. + 19. * random.random())
        elif np.abs(new_delta_energy) < self.divergence_threshold:
            self._calc_parameters()
            try:
                log_eps = (np.log(target_denergy) - self._a) / self._b
                new_val = np.exp(log_eps)
            except FloatingPointError:  # probably overflow (or new: self._b is zero)
                # value = inf will trigger the exceeded if clauses downhill
                new_val = np.inf
        else:  # divergent behavior detected, randomness to avoid loop like behavior
            new_val = self.val * (.9 + .1 * random.random())
        self.val = new_val

    def _calc_parameters(self):
        y = np.nanmean(self._log_denergy_hist)
        x = np.nanmean(self._log_eps_hist)
        dy = np.subtract(self._log_denergy_hist, y)
        dx = np.subtract(self._log_eps_hist, x)
        denominator = np.nansum(dx * dx)
        if denominator == 0.:
            # happens most likely only in the beginning
            b = 1.5 + random.random()
        else:
            b = np.nansum(dx * dy) / denominator
        self._a = y - b * x
        self._b = b

    def reset(self, start_val=None):
        super(EpsilonExponential, self).reset(start_val=start_val)
        self._log_denergy_hist = self._denergy_hist.copy()
        self._log_eps_hist = self._eps_hist.copy()
        self._a = 1.
        self._b = 2.


class EpsilonOneOverX(Epsilon):
    """
    class to handle adjusting the epsilon parameter in the integration.
    Epsilon gets adjusted by assuming a fitting function dEnergy(eps) = a * ((eps_0 - eps)**(-2) - eps_0**(-2))
    and fitting this to former (dEnergy, epsilon) pairs. With this fitting function the next epsilon can be chosen to
    give an dEnergy which would give approximately the target acceptance rate.
    """
    def __init__(self, target_acceptance_rate=0.8, limits=None):
        """
        Parameters
        ----------
        target_acceptance_rate : float
            wanted acceptance rate during sampling
        limits : list of float
            list with two elements containing minimum and maximum value of epsilon (default: [1.E-10, 10]
        """
        super(EpsilonOneOverX, self).__init__(target_acceptance_rate=target_acceptance_rate,
                                              limits=limits)
        self.a = 1.
        self.b = 2.
        self._epsilon0 = self.limits[1]
        warnings.simplefilter('error', OptimizeWarning)

    def _update_val(self, new_delta_energy):
        target_denergy = 2 * (1 - self._tar)
        if new_delta_energy == 0.:
            # epsilon way too small => make it much larger. Add randomness to tackle problems with divergent behavior
            self.val * (1. + 19. * random.random())
        else:
            if np.isinf(new_delta_energy):
                self._epsilon0 = self.val

            self._update_parameters()
            # update epsilon
            try:
                eps = self._epsilon0 * (1 - 1 / (target_denergy * self._epsilon0**self.b / self.a + 1)**(1/self.b))
            except FloatingPointError:  # probably overflow (or new: self._b is zero)
                # value = inf will be handled by _check_val()
                eps = np.inf
            self.val = eps

    def _update_parameters(self):
        x0 = self._epsilon0

        def fit_function(x, a, b):
            return a * (1 / (x0 - x) ** b - 1 / x0 ** b)

        if self._hist_index >= 4:
            x_data = self._eps_hist[np.logical_not(np.isnan(self._eps_hist))]
            y_data = self._denergy_hist[np.logical_not(np.isnan(self._denergy_hist))]
            try:
                new_a, new_b = tuple(curve_fit(fit_function, xdata=x_data, ydata=y_data, p0=[self.a, self.b],
                                     bounds=([1.E-10, 1.], [np.inf, 10.]))[0])
            except (RuntimeError, OptimizeWarning, FloatingPointError):  # curve_fit failed
                new_a, new_b = self.a, self.b
            self.a = new_a
            self.b = new_b

    def reset(self, start_val=None):
        super(EpsilonOneOverX, self).reset(start_val=start_val)
        self.a = 1.
        self.b = 2.
        self._epsilon0 = self.limits[1]
