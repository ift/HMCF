# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

"""
This module is kind of an extension to the basic HMCSampler - class. Due to pickling problems (especially under windows)
when using multiprocessing it is better to separate multiprocessing-related functions from the actual class, such that
their method-status is technically revoked (and the class and and its attributes (such as the logger) do not need to
be pickled during multiprocessing).
"""
from copy import copy
import traceback
import multiprocessing as mp
import random
from ..logging import logger, redirect_logging, reset_logging

import nifty4 as ift
import numpy as np

from .epsilon import *
from ..mass import MassChain


__all__ = ['generate_hmc_chain']


def generate_hmc_chain(transmitter, receiver, potential, chain_identifier=0, num_samples=100, max_burn_in=None,
                       random_seed=None, debug_mode=False, step_range=None, epsilon=None, order=2,
                       mass=None, x_initial=None):
    """
    generates a hmc markov chain

    Parameters
    ----------
    transmitter : mp.Queue
        samples and information to main process
    receiver : mp.Connection
        information from main process whether sampler has converged
    potential : ift.Energy
        hamiltonian potential used during sampling (i.e. -log(P(x)) where P(x) is the distribution of interest)
    chain_identifier : int
        identification number for chain. Default: 0.
    num_samples : int
        number of samples to be drawn after burn in. Default: 100.
    max_burn_in : int, optional
        maximum number of steps for the chain to converge before it is forced into sampling. If None then there is no
        restriction.
    random_seed : int, optional
        for multiprocessing the random generator gets copied from the parent process. Therefore, to have truly different
        Markov chains one needs to set different random seeds in the parent process.
    debug_mode : bool
        log delta_energy after each leapfrog step
    step_range : list of float
        defines the range of integration steps (picked uniformly). Default: [20, 30].
    epsilon : Epsilon
        an Epsilon class instance. Default: EpsilonSimple
    order : int
        The order of the symplectic integrator.
        For symmetry reasons only even integers are possible.
        order=2 equals a standard leapfrog integration.
    mass: MassChain
        handling the mass optimization thingy
    x_initial : ift.Field, optional
        starting point for hmc sampler, if None then a random field according to 'random_type' is generated.
    """
    try:
        chain = HMCChain(transmitter, receiver, potential, chain_identifier, num_samples, max_burn_in, step_range,
                         epsilon, order, mass, random_seed, debug_mode)

        chain.run(x_initial)
    except Exception as e:
        traceback_msg = traceback.format_exc()
        transmitter.put(dict(exception=e, traceback=traceback_msg))
        raise


class HMCChain(object):
    def __init__(self, transmitter, receiver, potential, chain_identifier=0, num_samples=100, max_burn_in=None,
                 step_range=None, epsilon=None, order=2, mass=None, random_seed=None,
                 debug_mode=False):
        """
        Parameters
        ----------
        transmitter : mp.Queue
            samples and information to main process
        receiver : mp.Connection
            information from main process whether sampler has converged
        potential : ift.Energy
            hamiltonian potential used during sampling (i.e. -log(P(x)) where P(x) is the distribution of
            interest)
        chain_identifier : int
            identification number for chain. Default: 0.
        num_samples : int
            number of samples to be drawn after burn in. Default: 100.
        max_burn_in : int, optional
            maximum number of steps for the chain to converge before it is forced into sampling. If None then
            there is no restriction.
        step_range : list of float
            defines the range of integration steps (picked uniformly). Default: [20, 30].
        epsilon : Epsilon
            an Epsilon class instance. Default: EpsilonSimple
        order : int
            The order of the symplectic integrator.
            For symmetry reasons only even integers are possible.
            order=2 equals a standard leapfrog integration.
        mass: MassChain
            handling the mass optimization thingy
        random_seed : int
            for multiprocessing the random generator gets copied from the parent process. Therefore, to have
            truly different Markov chains one needs to set different random seeds in the parent process.
        debug_mode : bool
            log delta_energy after each leapfrog step
                """
        self._identifier = chain_identifier
        if random_seed is None:
            random_seed = np.random.randint(10000000000)
        np.random.seed(random_seed)

        # run parameters
        self._num_samples = num_samples
        self._max_burn_in = max_burn_in
        if step_range is None:
            step_range = [20, 30]
        self._n_limits = step_range
        self._coeffs = _gen_coeff_list(order)
        self._converged = False
        self.step = 1
        self.debug_mode = debug_mode

        # HMC problem related
        self._potential = potential
        self._domain = self._potential.position.domain
        self._dtype = self._potential.position.dtype
        self.position = None  # type: ift.Field
        if epsilon is None:
            epsilon = EpsilonSimple()
        self._epsilon = epsilon
        if mass is None:
            mass = MassChain(potential.position)
        self._mass = mass

        # communication stuff
        self._transmitter = transmitter
        self._outgoing_package = dict(log=[], chain_identifier=self._identifier, dec_convergence_level=False,
                                      mass_reeval=False, epsilon=self._epsilon, num_mass_samples=0,
                                      new_sample=True, sample=self.position, accepted=False, step=self.step)
        self._receiver = receiver
        self._incoming_package = dict(converged=False, conv_level=0)

    def run(self, x_initial=None):
        """
        Parameters
        ----------
        x_initial : ift.Field, optional
            starting point for hmc sampler, if None then a random field according to 'random_type' is generated.
        """
        # tell numpy to raise errors if something went wrong
        np_err_old_settings = np.seterr(all='raise')

        # redirect logging messages
        old_hdlrs = redirect_logging(self._outgoing_package)

        self._init_chain()
        self._set_starting_point(x_initial=x_initial)
        self._burn_in()
        self._sampling()

        # reset numpy to old settings
        np.seterr(**np_err_old_settings)

        # reset logging settings (not sure whether necessary since subprocess is closed down anyway)
        reset_logging(*old_hdlrs)

    def _init_chain(self):
        """this is for inherited classes
        """
        pass

    def _set_starting_point(self, x_initial=None):
        """
        Parameters
        ----------
        x_initial : ift.Field
        """
        if x_initial is None:
            x_initial = self._potential.curvature.draw_sample()
        self.position = x_initial.copy()
        self._outgoing_package.update(sample=self.position)

    def _burn_in(self):
        self.step = 1
        self._outgoing_package.update(burn_in=True)

        while self._continue_burn_in():
            delta_energy = self._draw_sample()
            self._send()
            self._epsilon.update(delta_energy)
            self._receive()
            self._mass_update()
        logger.info('Burn-in phase finished. (step {:d})'.format(self.step))

    def _continue_burn_in(self):
        converged = self._converged and (self._incoming_package['conv_level'] == 0)
        continue_burn_in = (not (converged and self._epsilon.converged)) or self._mass.get_initial_mass
        if (self._max_burn_in is not None) and continue_burn_in:
            continue_burn_in = self.step < self._max_burn_in
            if not continue_burn_in:
                logger.warning('Reached maximum number of burn-in steps. Chain may have not converged.')
        return continue_burn_in

    def _sampling(self):
        self.step = 1
        self._outgoing_package.update(burn_in=False)

        while self.step <= self._num_samples:
            if self.debug_mode:
                self._draw_sample_debug()
            else:
                self._draw_sample()
            self._send()

    def _draw_sample(self):
        """
        Returns
        -------
        accepted : bool
            whether or not the new sample was accepted
        delta_energy : float
            integration error
        """
        # sample momentum from normal distribution
        p = self._sample_momentum()
        # integrate hamiltonian dynamics
        steps = random.randint(self._n_limits[0], self._n_limits[1])
        try:
            pos_final, p_final, pot_initial, pot_final = _symplectic_integrator(
                    x_initial=self.position, p_initial=p, mass=self._mass.operator, potential=self._potential,
                    coeffs=self._coeffs, epsilon=self._epsilon.val, steps=steps)
            new_pos, accepted, delta_energy = _acceptance(x_initial=self.position, p_initial=p, x_final=pos_final,
                                                          p_final=p_final, mass=self._mass.operator,
                                                          potential_initial=pot_initial, potential_final=pot_final)
        except FloatingPointError:
            delta_energy = np.inf
            logger.debug('FloatingPointError occurred during sampling.')
            self._outgoing_package.update(new_sample=False)
        else:
            self.position = new_pos
            self._outgoing_package.update(new_sample=True, sample=self.position, accepted=accepted, step=self.step)
            self.step += 1
        self._outgoing_package.update(delta_energy=delta_energy, epsilon=self._epsilon)
        return delta_energy

    def _draw_sample_debug(self):
        delta_energy_arr = np.zeros((self._n_limits[1] + 1,), dtype=float)
        integration_steps = random.randint(self._n_limits[0], self._n_limits[1])
        x_initial = self.position.copy()
        x_start = x_initial.copy()
        potential_initial = self._potential.at(x_initial)
        potential_start = potential_initial
        try:
            p_initial = self._sample_momentum()
            p_start = p_initial.copy()
            for i in range(integration_steps):
                x_stop, p_stop, _, potential_stop = _symplectic_integrator(
                        x_start, p_start, self._mass.operator, potential_start, self._coeffs, self._epsilon.val,
                        steps=1)
                _delta_energy = (_calc_energy_val(p_stop, self._mass.operator, potential_stop) -
                                 _calc_energy_val(p_start, self._mass.operator, potential_start))
                delta_energy_arr[i + 1] = _delta_energy
                x_start = x_stop.copy()
                p_start = p_stop.copy()
                potential_start = potential_stop
            x_final = x_start.copy()
            p_final = p_start.copy()
            potential_final = potential_start
            new_pos, accepted, delta_energy = _acceptance(x_initial, p_initial, x_final, p_final, self._mass.operator,
                                                          potential_initial, potential_final)
        except FloatingPointError:
            delta_energy = np.inf
            self._outgoing_package.update(new_sample=False)
        else:
            self.position = new_pos
            self._outgoing_package.update()
            self._outgoing_package.update(new_sample=True, sample=self.position, accepted=accepted,
                                          delta_energy=delta_energy, delta_energy_debug=copy(delta_energy_arr),
                                          epsilon=self._epsilon, step=self.step)
            self.step += 1
        return delta_energy

    def _sample_momentum(self):
        """
        samples momentum field from normal distribution (with mean=0 and
        covariance mass_op)
        Returns
        -------
        sampled_p_field : ift.Field
        """
        return self._mass.operator.draw_sample()

    def _send(self):
        self._transmitter.put(self._outgoing_package.copy())
        self._outgoing_package.update(log=[])

    def _receive(self):
        if self._receiver.poll(10.):
            self._incoming_package = self._receiver.recv()
            self._converged = self._incoming_package['converged']
        else:
            self._converged = False

    def _mass_update(self):
        new_mass_flag = self._mass.update(self._incoming_package, self._outgoing_package)
        if new_mass_flag:
            self._epsilon.reset(start_val=self._epsilon.val)


def _get_coeffs(order):
    if order % 2 != 0:
        raise ValueError('The order of the symplectic integrator has to be even')
    n = (order / 2.) - 1
    b = np.exp(np.log(2) / (2*n+1))
    z_0 = - b / (2 - b)
    z_1 = 1. / (2 - b)
    return z_0, z_1


def _gen_coeff_list(order):
    if order == 2:
        return np.array([0.5, 1., 0.5])
    else:
        z_0, z_1 = _get_coeffs(order)
        coeff_list = _gen_coeff_list(order-2)
        first = z_1*coeff_list[:-1]
        bridge = [coeff_list[0]*(z_0 + z_1)]
        mid = z_0*coeff_list[1:-1]

        return np.concatenate((first, bridge, mid, bridge, first[::-1]))


def _symplectic_integrator(x_initial, p_initial, mass, potential, coeffs, epsilon=0.005, steps=1):
    """
    Simple higher order symplectic integrators.
    Coefficients can be generated with _gen_coeff_list.
    For a standard leapfrog integration the coefficients would read [0.5, 1., 0.5].
    Follows the analytic (and less efficient) approach in "Construction of higher order symplectic integrators" by
    Haruo Yoshida, Phys. Let. A.

    Parameters
    ----------
    x_initial : ift.Field
        initial configuration in x-space
    p_initial : ift.Field
        initial configuration in p-space
    mass : CustomMassOperator
    potential : ift.Energy
        HMC potential (at any point)
    coeffs : np.ndarray
        coefficients for symplectic integration
    epsilon : float
        numerical integration step width
    steps : int
        number of integration steps

    Returns
    -------
    x_final : ift.Field
        final x_field
    p_final : ift.Field
        final p_field
    old_potential : ift.Energy
        potential at initial x_field
    new_potential : ift.Energy
        potential at final x_field
    """
    p = p_initial.copy()
    x = x_initial.copy()
    potential = potential.at(x)
    _potential = potential.at(x)
    for i in range(steps):
        delta_p = - epsilon * coeffs[0] * _potential.gradient
        p = p + delta_p

        for x_coeff, p_coeff in zip(coeffs[1::2], coeffs[2::2]):
            delta_x = epsilon * x_coeff * mass.inverse_times(p)
            x = x + delta_x  # type: ift.Field
            _potential = _potential.at(x)
            delta_p = - epsilon * p_coeff * _potential.gradient
            p = p + delta_p
    return x.copy(), p.copy(), potential, _potential


def _calc_energy_val(p_field, mass, potential):
    """
    calculates the energy for given fields x_field, p_field and HMC -
    Hamiltonian

    Parameters
    ----------
    p_field : ift.Field
    mass : CustomMassOperator
    potential : ift.Energy
        HMC potential at position of evaluation

    Returns
    -------
    energy_val : float
        The value of the Hamiltonian at the point in phase space given by
        position of potential and p_field.
    """
    # V(x)+0.5 x p*M⁻¹p
    energy_val = potential.value + 0.5 * (p_field.vdot(mass.inverse_times(p_field)))
    return float(energy_val)


def _acceptance(x_initial, p_initial, x_final, p_final, mass, potential_initial,
                potential_final):
    """
    checks whether new points in parameter space are accepted or not with
    min(1, exp(-(H_new-H_old)))
    (and discards p-configuration in any case)

    Parameters
    ----------
    x_initial : ift.Field
        starting configuration in x-space of current iteration
    p_initial : ift.Field
        starting configuration in p-space of current iteration
    x_final : ift.Field
        configuration in x-space after integration
    p_final : ift.Field
        configuration in p-space after integration
    mass : CustomMassOperator
    potential_initial : ift.Energy
        HMC potential at initial x_field
    potential_final : ift.Energy
        HMC potential at final x_field

    Returns
    ----------
    x_field : ift.Field
        new configuration in x-space
    accepted : bool
        whether or not the new sample was accepted
    delta_energy : float
        integration error
    """
    limit = 1.E100
    old_energy = _calc_energy_val(p_initial, mass, potential_initial)
    new_energy = _calc_energy_val(p_final, mass, potential_final)
    delta_energy = new_energy - old_energy
    if np.abs(delta_energy) > limit or np.isnan(delta_energy):
        #  divergent behavior
        accepted = False
        result = x_initial.copy()
    elif delta_energy <= 0.:
        accepted = True
        result = x_final.copy()
    else:
        acc_prob = np.exp(-delta_energy)
        rand_frac = np.random.rand()
        if rand_frac < acc_prob:
            accepted = True
            result = x_final.copy()
        else:
            accepted = False
            result = x_initial.copy()
    return result, accepted, delta_energy


def _nuts_check(x_initial, x_current, p_current):
    """
    a check during leapfrog whether or not the trajectory made a u-turn and now approaches the inital position

    Parameters
    ----------
    x_initial : ift.Field
    x_current : ift.Field
    p_current : ift.Field

    Returns
    -------
    no_u_turn : bool
        whether or not the trajectory is going to make a u-turn in the next infinitesimal time step
    """
    away_measure = (x_current - x_initial).vdot(p_current)
    print(away_measure)
    return away_measure >= 0
