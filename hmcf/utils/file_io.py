# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

import os
from re import compile as re_compile
from re import search, match
import numpy as np
import h5py
from datetime import datetime
import nifty4 as ift
from ..logging import logger


__all__ = ['load', 'load_mean', 'load_var']


_h5_file_name_re = re_compile(r'\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}')
_chain_dir_name_re = re_compile(r'chain\d{4}$')


def _get_recent_file_path(dir_path):
    date_obj_list = []
    full_path = os.path.abspath(dir_path)
    for file_name in os.listdir(full_path):
        search_obj = search(_h5_file_name_re, file_name)
        if search_obj:
            date_obj_list.append(datetime.strptime(search_obj.group(), '%Y-%m-%d_%H-%M-%S'))
    if len(date_obj_list) == 0:
        raise ValueError('Directory does not contain any run-files:', full_path)
    file_name = 'run' + max(date_obj_list).strftime('%Y-%m-%d_%H-%M-%S') + '.h5'
    return os.path.join(dir_path, file_name)


def _get_field_meta(h5_file, attr_type):
    """

    Parameters
    ----------
    h5_file : h5py.File
    attr_type : str

    Returns
    -------
    meta : dict
        meta_information for each element in attr_type as a tuple where
        1st element : (num_chains, max_num_samples, field_shape[0], field_shape[1], ...)
        2nd element : dtype
    """
    highest_index = -1
    max_num_samples = 0
    field_shapes = dict()
    field_dtypes = dict()

    chains = [(name, chain) for name, chain in h5_file.items() if match(_chain_dir_name_re, name)]

    for name, chain in chains:
        chain_index = int(search(r'\d+', name).group())
        attribute = chain[attr_type]
        for key, field_val in attribute.items():
            num_samples = field_val.shape[0]
            # the following line is actually only needed one time
            field_shapes.update({key: field_val.shape[1:]})
            field_dtypes.update({key: field_val.dtype})
            if num_samples > max_num_samples:
                max_num_samples = num_samples
        if chain_index > highest_index:
            highest_index = chain_index

    shapes = {key: ((highest_index+1, max_num_samples)+shape, field_dtypes[key]) for key, shape in field_shapes.items()}
    return shapes


def _load_single_field(field_name, h5_file, attr_type, field_meta_data, start=0, stop=None, step=1):
    max_num_samples = field_meta_data[0][1]

    # check and adjust start, stop, step values
    if (stop is None) or (stop > max_num_samples):
        stop = max_num_samples
    num_samples = int(np.ceil(float(stop - start) / step))
    if num_samples <= 0:
        logger.warning('start/stop/step such that no samples will be returned')
        return np.full((field_meta_data[0][0],)+field_meta_data[0][1:], np.nan, dtype=field_meta_data[1])

    # everything alright, start loading the data from the h5df file
    shape = (field_meta_data[0][0], num_samples) + field_meta_data[0][2:]
    field_val = np.full(shape, np.nan, dtype=field_meta_data[1])
    for chain_name, chain_data in h5_file.items():
        index = int(search(r'\d+', chain_name).group())
        chain_data_chunk = chain_data[attr_type][field_name][start:stop:step]
        field_val[index][:chain_data_chunk.shape[0]] = chain_data_chunk
        logger.debug('loaded chain ' + str(index) + ' for field ' + field_name)
    return field_val


def load(path, attr_type='samples', start=0, stop=None, step=1):
    """
    loads the specified attr_type from an h5py file and returns it in an 2+n dim np.ndarray, where the first two
    dimensions represent chain- and sample-index respectively and the other n dimensions the underlying shape of
    the sampled result field

    Parameters
    ----------
    attr_type : str
        valid entries for HMCSampler file:      'samples', 'burn_in'
        valid entries for HMCSamplerDebug file: 'samples', 'burn_in', 'raw_samples', 'raw_burn_in', 'gradients',
        'pot_vals', 'conv_meas'
    path : str, optional
        path to h5 file, or run file directory.
        In case of the latter, the most recent h5 file is loaded.
    start : int, optional
    stop : int, optional
    step : int, optional

    Returns
    -------
    attribute : np.ndarray, dict
        if the stored file contains MultiFields, a dictionary of numpy arrays is returned with the original MultiField
        names as keys.
    """
    if os.path.isdir(path):
        path = _get_recent_file_path(path)
        logger.debug('path is not a file. Load most recent file instead: ' + path)

    result_field = dict()
    f = h5py.File(path, 'r', libver='latest')
    field_meta_data = _get_field_meta(f, attr_type)

    # essentially iterate through different fields in (possibly) multi field
    for key, meta in field_meta_data.items():
        field_val = _load_single_field(field_name=key, h5_file=f, attr_type=attr_type, field_meta_data=meta,
                                       start=start, stop=stop, step=step)
        result_field.update({key: field_val})
    f.close()

    # check whether actually not a multi field
    if (len(result_field) == 1) and ('ift_field' in result_field.keys()):
        return result_field['ift_field']
    return result_field


def _load_single_field_mean(h5_file, field_name, field_meta_data, domain=None):
    chunk_size = 500
    weights = [0, 0]
    mean_val = np.zeros(field_meta_data[0][2:], dtype=field_meta_data[1])

    for name, chain_data in h5_file.items():
        if _chain_dir_name_re.match(name):
            index = int(search(r'\d+', name).group())
            chain_samples = chain_data['samples'][field_name]  # type: h5py.Dataset
            for i in range(0, chain_samples.shape[0], chunk_size):
                chunk = chain_samples[i:i + chunk_size]
                weights[1] = chunk.shape[0]
                chunk_mean = np.mean(chunk, axis=0)
                mean_val = np.average([mean_val, chunk_mean], axis=0, weights=weights)
                weights[0] += weights[1]
            logger.debug('processed chain ' + str(index) + 'for mean value calculation.')

    if domain is None:
        return mean_val
    if isinstance(domain, ift.MultiDomain):
        domain = domain[field_name]
    return ift.Field(domain=domain, val=mean_val)


def load_mean(path, domain=None):
    """
    Parameters
    ----------
    path : str
        path to h5 file, or run file directory.
        In case of the latter, the most recent h5 file is loaded.
    domain : ift.Domain, ift.MultiDomain, optional

    Returns
    -------
    mean_val : np.ndarray, ift.Field, dict, ift.MultiField
        the field values depending on whether there is a Field or a MultiField in the h5df file and whether a domain
        is given.
    """
    if os.path.isdir(path):
        path = _get_recent_file_path(path)
        logger.debug('path is not a file. Load most recent file instead: ' + path)
    f = h5py.File(path, 'r', libver='latest')

    fields_meta_data = _get_field_meta(f, attr_type='samples')

    num_samples = next(iter(fields_meta_data.values()))[0][1]
    if num_samples == 0:
        raise ValueError("No samples found in file (run may have never converged).")

    mean = dict()
    for key, meta_data in fields_meta_data.items():
        single_mean = _load_single_field_mean(h5_file=f, field_name=key, field_meta_data=meta_data, domain=domain)
        mean.update({key: single_mean})
    f.close()

    if (len(mean) == 1) and ('ift_field' in mean.keys()):
        mean = mean['ift_field']
    elif domain is not None:
        mean = ift.MultiField(val=mean)
    return mean


def _load_single_field_var(h5_file, field_name, field_meta_data, domain=None):
    chunk_size = 500  # avoiding heavy memory usage for large sampling sizes
    num_samples = field_meta_data[0][0] * field_meta_data[0][1]
    sum_samples_squared = np.zeros(field_meta_data[0][2:], dtype=field_meta_data[1])

    for chain_name, chain_data in h5_file.items():
        if _chain_dir_name_re.match(chain_name):
            index = int(search(r'\d+', chain_name).group())
            chain_samples = chain_data['samples'][field_name]  # type: h5py.Dataset
            for i in range(0, chain_samples.shape[0], chunk_size):
                squared_chunk = (chain_samples[i:i + chunk_size])**2 / (num_samples - 1)  # type: np.ndarray
                sum_samples_squared += np.sum(squared_chunk, axis=0)
            logger.debug('processed chain ' + str(index) + 'for variance value calculation.')

    if domain is not None:
        if isinstance(domain, ift.MultiDomain):
            domain = domain[field_name]
        sum_samples_squared = ift.Field(domain=domain, val=sum_samples_squared)
    mean_val = _load_single_field_mean(h5_file=h5_file, field_name=field_name, field_meta_data=field_meta_data,
                                       domain=domain)
    var = sum_samples_squared - num_samples / (num_samples - 1) * mean_val**2

    return var


def load_var(path, domain=None):
    """
    loads an h5py file and calculates the variance of the samples

    Parameters
    ----------
    path : str
        path to h5 file, or run file directory.
        In case of the latter, the most recent h5 file is loaded.
    domain : ift.Domain, optional

    Returns
    -------
    variance : np.ndarray, ift.Field, dict, ift.MultiField
        the field values depending on whether there is a Field or a MultiField in the h5df file and whether a domain
        is given.
    """
    if os.path.isdir(path):
        path = _get_recent_file_path(path)
        logger.debug('path is not a file. Load most recent file instead: ' + path)

    f = h5py.File(path, 'r', libver='latest')
    fields_meta_data = _get_field_meta(h5_file=f, attr_type='samples')

    num_samples = next(iter(fields_meta_data.values()))[0][1]
    if num_samples == 0:
        raise ValueError("No samples found in file (run may have never converged).")

    var = dict()
    for key, meta_data in fields_meta_data.items():
        single_var = _load_single_field_var(h5_file=f, field_name=key, field_meta_data=meta_data, domain=domain)
        var.update({key: single_var})
    f.close()

    if (len(var) == 1) and ('ift_field' in var.keys()):
        var = var['ift_field']
    elif domain is not None:
        var = ift.MultiField(val=var)
    return var
