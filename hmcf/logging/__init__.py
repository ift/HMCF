import logging

logger = logging.getLogger('HMCF')

formatter = logging.Formatter('{asctime} - {name} - {levelname}: {message}', style='{')

logger.setLevel(logging.DEBUG)
_hdlr = logging.StreamHandler()
_hdlr.setLevel(logger.level)
_hdlr.setFormatter(formatter)
logger.addHandler(_hdlr)

from .hmcf_logger import *
