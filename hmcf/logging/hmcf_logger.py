from logging.handlers import QueueHandler, BufferingHandler
from . import logger
import nifty4 as ift


__all__ = ['HMCLogHandler', 'handle_logs', 'redirect_logging', 'reset_logging', 'DisplayHandler']


class HMCLogHandler(QueueHandler):
    """ A class for handling sub-process logging messages.

    This logging.Handler deals with logging messages as any other Handler (e.g. it can be combined with a
    logging.Formatter and so on) but appends the output to the queue packages which are send from the sup-process to the
    main process.
    This is particular important for logging messages coming from the nifty package.

    Attributes
    ----------
    queue : dict
        Package for data transfer between sub-process and main process during an HMC run.

    """
    def __init__(self, out_pkg):
        """
        Parameters
        ----------
        out_pkg : dict
            Package for data transfer between sub-process and main process during an HMC run.
        """
        super(HMCLogHandler, self).__init__(queue=out_pkg)

    def enqueue(self, record):
        if 'log' not in self.queue.keys():
            self.queue['log'] = []
        self.queue['log'].append(record)


def handle_logs(package):
    """
    Handles the log messages in the multi-process communication.

    Parameters
    ----------
    package : dict
        The queue packages interchanged between sub- and main processes.
    """
    logs = package['log']
    ch_id = package['chain_identifier']
    for record in logs:
        record.msg = 'chain{:03d}: '.format(ch_id) + record.msg
        logger.handle(record)


def redirect_logging(package):
    """ Takes care of redirecting logging messages from nifty and hmcf during sampling in sup-processes.

    Parameters
    ----------
    package : dict
        Package of interest (used for exchange of information between sub- and main processes).

    Returns
    -------
    old_nifty_hdlrs : list
        For resetting.
    old_hmcf_hdlrs : list
        For resetting.
    """
    loggers = [logger, ift.logger]
    old_hdlrs = []
    new_hdlrs = [HMCLogHandler(package) for _ in range(2)]
    for index, log in enumerate(loggers):
        handlers = log.handlers
        for hdlr in handlers:
            log.removeHandler(hdlr)
        new_hdlrs[index].setLevel(log.level)
        log.addHandler(new_hdlrs[index])
        old_hdlrs.append(handlers)
    return tuple(old_hdlrs)


def reset_logging(old_nifty_hdlrs, old_hmcf_hdlrs):
    """ Resets the redirection of logging in sub-processes.

    Parameters
    ----------
    old_nifty_hdlrs : list
    The NIFTy logging handlers before redirection (only necessary if reset=True)
    old_hmcf_hdlrs : list
    The HMCF logging handlers before redirection (only necessary if reset=True)
    """
    loggers = [ift.logger, logger]
    old_hdlrs = [old_nifty_hdlrs, old_hmcf_hdlrs]
    for index, log in enumerate(loggers):
        hdlr = log.handlers[0]
        log.removeHandler(hdlr)
        for hdlr in old_hdlrs[index]:
            log.addHandler(hdlr)


class DisplayHandler(BufferingHandler):
    def emit(self, record):
        """
        Emit a record.

        Append the record. If shouldFlush() tells us to, call flush() to process
        the buffer.

        The important difference to BufferingHandler is, that the buffer does not contain records but formatted strings.
        """
        self.buffer.append(self.format(record))
        if self.shouldFlush(record):
            self.flush()
            logger.warning('Display logging handler overflow. Flushed all messages.')

