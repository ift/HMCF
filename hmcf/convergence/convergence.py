# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik

import numpy as np
import nifty4 as ift


__all__ = ['Convergence', 'MeanConvergence', 'SimpleVarianceConvergence', 'HansonConvergence', 'GelmanRubinConvergence',
           'MultiConvergence']


class Convergence(object):
    """
    abstract class for different convergences (based on different convergence criteria). A chain is said to have
    converged if max(convergence_measure) < tolerance * 10**level
    """

    def __init__(self, domain, dtype=np.float64, num_chains=1, tolerance=2., level=1):
        """
        Parameters
        ----------
        domain : ift.DomainTuple
        dtype : np.dtype.dtype
        num_chains : int
            number of MCMC chains
        tolerance : float
            tolerance * 10**level defines the upper bound for 'converged'
        level : int
            see tolerance
        """
        self._locality = 250
        self._shape = domain.shape
        self._dtype = dtype
        self._num_chains = num_chains
        self._tolerance = tolerance
        self._level = np.full(self._num_chains, level, dtype=int)
        self._samples = np.zeros((self._num_chains, self._locality) + self._shape, dtype=self._dtype)
        self._samples_index = np.zeros(self._num_chains, dtype=int)
        self._num_samples = np.zeros(self._num_chains, dtype=int)
        self._measure = np.full((self._num_chains,) + self._shape, np.inf, dtype=float)
        self._measure_max = np.full(self._num_chains, np.inf, dtype=float)
        self._converged = np.full(self._num_chains, False, dtype=bool)
        self._quota = np.full(self._num_chains, 0., dtype=float)

    @property
    def tolerance(self):
        """float: a chain is said to have converged if the maximum convergence measure is smaller than
        tolerance * 10**level"""
        return self._tolerance

    @tolerance.setter
    def tolerance(self, value):
        self._tolerance = value
    
    @property
    def locality(self):
        """int : defines the number of samples to be considered for the convergence criteria. Default: 250.
        Setting locality resets the instance (except for convergence level and tolerance)"""
        return self._locality

    @locality.setter
    def locality(self, value):
        self._locality = value
        self.reset()

    @property
    def level(self):
        """np.ndarray of int: a chain is said to have converged if the maximum convergence measure is smaller than
        tolerance * 10**level. If setting with a bare int, all chains get the same value."""
        return self._level

    @level.setter
    def level(self, value):
        if not hasattr(value, '__iter__'):
            self._level = np.full(self._num_chains, value, dtype=int)
        else:
            self._level = np.array(value, dtype=int)

    @property
    def measure(self):
        """np.ndarray: convergence measure for each chain, calculated during 'update'"""
        return self._measure

    @property
    def measure_max(self):
        return self._measure_max

    @property
    def converged(self):
        """list(bool): statements for each chain whether or not it has converged"""
        return self._converged

    @property
    def quota(self):
        """list(float): the proportion of converged pixels"""
        return self._quota

    def dec_level(self, chain_identifier=None):
        if chain_identifier is None:
            nonzero_mask = self._level > 0
            self._level[nonzero_mask] -= 1
        else:
            if self._level[chain_identifier] > 0:
                self._level[chain_identifier] -= 1

    def inc_level(self, chain_identifier=None):
        if chain_identifier is None:
            self._level += 1
        else:
            self._level[chain_identifier] += 1

    def update(self, sample, chain_identifier=0,  *args, **kwargs):
        """
        When deriving a child class, implement _update_measure!

        Parameters
        ----------
        sample: ift.Field
            sampled field
        chain_identifier: int
        """
        ch_id = chain_identifier
        try:
            self._update_measure(sample, ch_id, *args, **kwargs)
        except FloatingPointError:
            self._measure[ch_id] = np.full_like(self._measure[ch_id], np.inf)

        self._measure_max[ch_id] = self._measure[ch_id].max()
        self._converged[ch_id] = self._measure_max[ch_id] <= self._tolerance * 10. ** self._level[ch_id]
        converged_pixels_mask = self._measure[ch_id] <= self._tolerance * 10. ** self._level[ch_id]  # type: np.ndarray
        self._quota[ch_id] = (np.sum(converged_pixels_mask, dtype=float) / np.prod(sample.shape))

    def _update_measure(self, sample, chain_identifier=0, *args, **kwargs):
        """
        when deriving a child class, calculate and update _measure[chain_identifier] in here

        Parameters
        ----------
        sample: ift.Field
            sampled field
        chain_identifier: int
        """
        raise NotImplementedError("No instance method '_update_measure'.")

    def _append_sample(self, sample, chain_identifier=0):
        ch_id = chain_identifier
        self._samples[ch_id][self._samples_index] = sample.val
        self._num_samples[ch_id] += 1
        self._samples_index[ch_id] = (self._samples_index[ch_id] + 1) % self._locality

    def reset(self, level=None):
        """
        resets convergence calculation to default.

        Parameters
        ----------
        level : int, array_like of int
        """
        if level is not None:
            if isinstance(level, int):
                self._level = np.full(self._num_chains, level, dtype=int)
            else:
                if self._level.shape == level.shape:
                    self._level = level
                else:
                    raise ValueError("not a valid value for 'level'")
        shape = self._shape
        dtype = self._dtype
        self._samples = np.zeros((self._num_chains, self._locality) + shape, dtype=dtype)
        self._samples_index = np.zeros(self._num_chains, dtype=int)
        self._num_samples = np.zeros(self._num_chains, dtype=int)
        self._measure = np.full((self._num_chains,) + shape, np.inf, dtype=dtype)
        self._converged = np.full(self._num_chains, False, dtype=bool)
        self._quota = np.full(self._num_chains, 0., dtype=float)


class MeanConvergence(Convergence):
    """
    Compares mean value of different chains to mean value of all samples from all chains. Converged if ratio of both
    minus one is smaller than tolerance * 10**level.
    """

    def __init__(self, domain, dtype=np.float64, num_chains=1, tolerance=2., level=0):
        if num_chains < 2:
            raise ValueError("More than one chain is needed for 'mean convergence' as convergence criteria.")
        super(MeanConvergence, self).__init__(domain=domain, dtype=dtype, num_chains=num_chains, tolerance=tolerance,
                                              level=level)
        self._mean = np.zeros(self._shape, dtype=self._dtype)
        self._means = np.zeros((self._num_chains,)+self._shape, dtype=self._dtype)

    @property
    def total_mean(self):
        return self._mean

    @property
    def chain_means(self):
        return self._means

    def _update_measure(self, sample, chain_identifier=0, *args, **kwargs):
        """
        Parameters
        ----------
        sample: ift.Field
            sampled field
        chain_identifier: int
        """
        ch_id = chain_identifier
        self._append_sample(sample, chain_identifier=ch_id)
        if (self._num_samples > 2.).all():
            self._means[ch_id] = np.mean(self._samples[ch_id][:self._num_samples[ch_id]], axis=0)
            self._mean = np.mean(np.concatenate(self._samples[:, :self._num_samples], axis=0), axis=0)
            self._measure[ch_id] = np.abs(self._means[ch_id] / self._mean - 1.)

    def reset(self, level=None):
        super(MeanConvergence, self).reset(level=level)
        self._mean = np.zeros(self._shape, dtype=self._dtype)
        self._means = np.zeros((self._num_chains,)+self._shape, dtype=self._dtype)


class SimpleVarianceConvergence(Convergence):
    """
    Compares variance of different chains to variance of all samples from all chains. Converged if ratio of both
    minus one is smaller than tolerance * 10**level.
    """

    def __init__(self, domain, dtype=np.float64, num_chains=1, tolerance=2., level=0):
        if num_chains < 2:
            raise ValueError("More than one chain is needed for 'simple variance convergence' as convergence criteria.")
        super(SimpleVarianceConvergence, self).__init__(domain=domain, dtype=dtype, num_chains=num_chains,
                                                        tolerance=tolerance, level=level)
        self._variance = np.zeros(self._shape, dtype=self._dtype)
        self._variances = np.zeros((self._num_chains,)+self._shape, dtype=self._dtype)

    @property
    def total_variance(self):
        return self._variance

    @property
    def chain_variances(self):
        return self._variances

    def _update_measure(self, sample, chain_identifier=0, *args, **kwargs):
        """
        Parameters
        ----------
        sample: ift.Field
            sampled field
        chain_identifier: int
        """
        ch_id = chain_identifier
        self._append_sample(sample, chain_identifier=ch_id)
        if (self._num_samples > 2.).all():
            self._variances[ch_id] = np.var(self._samples[ch_id][:self._num_samples[ch_id]], axis=0)
            self._variance = np.var(np.concatenate([self._samples[ch_index, :self._num_samples[ch_index]]
                                                    for ch_index in range(self._num_chains)], axis=0), axis=0)
            self._measure[ch_id] = np.abs(self._variance / self._variances[ch_id] - 1.)

    def reset(self, level=None):
        super(SimpleVarianceConvergence, self).reset(level=level)
        self._variance = np.zeros(self._shape, dtype=self._dtype)
        self._variances = np.zeros((self._num_chains,)+self._shape, dtype=self._dtype)


class HansonConvergence(Convergence):

    def __init__(self, domain, dtype=np.float64, num_chains=1, tolerance=2., level=0):
        super(HansonConvergence, self).__init__(domain=domain, dtype=dtype, num_chains=num_chains, tolerance=tolerance,
                                                level=level)
        self._sample_gradients = self._samples.copy()
        self._variances = self._measure.copy()
        self._hanson_variances = self._measure.copy()

    @property
    def variances(self):
        return self._variances

    @property
    def hanson_variances(self):
        return self._hanson_variances

    def _update_measure(self, sample, chain_identifier=0, *args, **kwargs):
        """

        Parameters
        ----------
        sample: ift.Field
            sampled field
        chain_identifier: int
            chain for which convergence measure is updated
        \**kwargs:
            see below

        Keyword Arguments
        -----------------
        sample_gradient: ift.Field
            gradient of distribution at sample
        """
        ch_id = chain_identifier
        self._append_sample_gradient(kwargs['sample_gradient'], chain_identifier=ch_id)
        self._append_sample(sample, chain_identifier=ch_id)
        if self._num_samples[ch_id] > 2.:
            self._variances[ch_id] = np.var(self._samples[ch_id, :self._num_samples[ch_id]], axis=0)
            self._update_hanson_var(chain_identifier=ch_id)
            convergence_measure = np.abs(self._variances[ch_id] / self._hanson_variances[ch_id] - 1.)
            self._measure[ch_id] = convergence_measure

    def _append_sample_gradient(self, sample_gradient, chain_identifier=0):
        ch_id = chain_identifier
        self._sample_gradients[ch_id][self._samples_index[ch_id]] = sample_gradient.val

    def _update_hanson_var(self, chain_identifier=0):
        """
        Parameters
        ----------
         chain_identifier: int

        Returns
        -------
        hanson_variance: np.ndarray
            variance calculated as Hanson K.M., 2001, Proc. SPIE 4322, 456
        """
        ch_id = chain_identifier
        summands = (self._samples[ch_id] - np.mean(self._samples[ch_id], axis=0))**3 * self._sample_gradients[ch_id]
        self._hanson_variances[ch_id] = (1. / 3. * np.mean(summands[:self._num_samples[ch_id]], axis=0))

    def reset(self, level=None):
        super(HansonConvergence, self).reset()
        self._sample_gradients = self._samples.copy()
        self._variances = np.zeros((self._num_chains,)+self._shape, dtype=self._dtype)
        self._hanson_variances = self._variances.copy()


class GelmanRubinConvergence(Convergence):

    def __init__(self, domain, dtype=np.float64, num_chains=1, tolerance=2., level=0):
        # if num_chains < 2:
        #     raise ValueError("More than one chain is needed for 'Gelman Rubin convergence' as convergence criteria.")
        super(GelmanRubinConvergence, self).__init__(domain=domain, dtype=dtype, num_chains=num_chains,
                                                     tolerance=tolerance, level=level)
        self._means = self._measure.copy()
        self._variances = self._measure.copy()

    def _update_measure(self, sample, chain_identifier=0, *args, **kwargs):
        """Gelman Rubin statistics. See Gelman Rubin 'Inference from Iterative Simulation Using Multiple Sequences'
        1992, without df/student t

        Parameters
        ----------
        sample: ift.Field
            sampled field
        chain_identifier: int
        """
        ch_id = chain_identifier
        self._append_sample(sample, chain_identifier=ch_id)
        if self._num_samples[ch_id] > 2:
            self._means[ch_id] = np.mean(self._samples[ch_id][:self._num_samples[ch_id]], axis=0)
            self._variances[ch_id] = np.var(self._samples[ch_id][:self._num_samples[ch_id]], axis=0)
        if (self._num_samples > 2).all():
            n = np.max([np.max(self._num_samples), self._locality])
            bn = np.var(self._means, axis=0)
            w = np.mean(self._variances, axis=0)
            tot_var = (n-1)/n * w + bn
            scale = tot_var + bn/self._num_chains
            self._measure[ch_id] = np.abs(scale/w - 1.)

    def reset(self, level=None):
        super(GelmanRubinConvergence, self).reset(level=level)
        self._means = self._measure.copy()
        self._variances = self._measure.copy()


class MultiConvergence(object):
    """
    Class for multi convergences (based on different convergence criteria). A chain is said to have
    converged if max(convergence_measure) < tolerance * 10**level
    """

    def __init__(self, domain, dtype=None, num_chains=1, tolerance=2., level=1, convergences=None):
        """
        Parameters
        ----------
        domain : ift.MultiDomain
        dtype : np.dtype.dtype
            Default: np.float64 for all sub fields.
        num_chains : int
            number of MCMC chains
        tolerance : float
            tolerance * 10**level defines the upper bound for 'converged'
        level : int
            see tolerance
        convergences : dict of str: Convergence, optional
            Tuple of (possibly different) convergence classes for multi fields
        """
        self._domain = domain
        self._keys = self._domain.keys()
        if dtype is None:
            dtype = {key: np.float64 for key in self._keys}
        self._dtype = dtype
        self._num_chains = num_chains

        if convergences is None:
            convergences = self._get_default_convergences(tolerance=tolerance, level=level)
        self._convergences = convergences

    @property
    def keys(self):
        return self._keys

    @property
    def tolerance(self):
        """float: a chain is said to have converged if the maximum convergence measure is smaller than
        tolerance * 10**level"""
        return (next(iter(self._convergences.values()))).level

    @tolerance.setter
    def tolerance(self, value):
        for key in self._keys:
            self._convergences[key].tolerance = value

    @property
    def locality(self):
        """int : defines the number of samples to be considered for the convergence criteria. Default: 250.
        Setting locality resets the instance (except for convergence level and tolerance)"""
        return (next(iter(self._convergences.values()))).level

    @locality.setter
    def locality(self, value):
        for key in self._keys:
            self._convergences[key].locality = value

    @property
    def level(self):
        """np.ndarray of int: a chain is said to have converged if the maximum convergence measure is smaller than
        tolerance * 10**level. If setting with a bare int, all chains get the same value."""
        # should be the same for every field in multifield, therefore just any
        return (next(iter(self._convergences.values()))).level

    @level.setter
    def level(self, value):
        for key in self._keys:
            self._convergences[key].level = value

    @property
    def measure(self):
        """dict of str: np.ndarray : convergence measure for each chain, calculated during 'update'"""
        return {key: convergence.measure for key, convergence in self._convergences.items()}

    @property
    def measure_max(self):
        """np.ndarray of float : maximum value of all convergence measures by chain"""
        return np.max(np.array([convergence.measure_max for convergence in self._convergences.values()]), axis=0)

    @property
    def converged(self):
        """np.ndarray of bool: statements for each chain whether or not all it multi fields have converged"""
        conv_sum = np.zeros(self._num_chains, dtype=int)
        num_conv = 0
        for key in self._keys:
            num_conv += 1
            conv_sum += self._convergences[key].converged
        return conv_sum == num_conv

    @property
    def quota(self):
        """list(float): the proportion of converged pixels"""
        quota_sum = np.zeros(self._num_chains, dtype=float)
        weight_sum = 0
        for key in self._keys:
            weight = np.prod(self._convergences[key]._shape)
            quota_sum += weight * self._convergences[key].quota
            weight_sum += weight
        return quota_sum / weight_sum

    @property
    def items(self):
        return self._convergences

    def dec_level(self, chain_identifier=None):
        for key in self._keys:
            self._convergences[key].dec_level(chain_identifier=chain_identifier)

    def inc_level(self, chain_identifier=None):
        for key in self._keys:
            self._convergences[key].inc_level(chain_identifier=chain_identifier)

    def update(self, sample, sample_gradient=None, chain_identifier=0):
        """
        when deriving a child class, implement _update_measure!
        Parameters
        ----------
        sample: gn.MultiField
            sampled MultiField
        sample_gradient : gn.MultiField
            associated gradient
        chain_identifier: int
        """
        for key in self._keys:
            self._convergences[key].update(sample[key], sample_gradient=sample_gradient[key],
                                           chain_identifier=chain_identifier)

    def _get_default_convergences(self, tolerance=2., level=0):
        if self._num_chains == 1:
            result = {key: HansonConvergence(domain=domain, dtype=self._dtype[key], num_chains=self._num_chains,
                                             tolerance=tolerance, level=level)
                      for key, domain in self._domain.items()}
        else:
            result = {key: GelmanRubinConvergence(domain=domain, dtype=self._dtype[key], num_chains=self._num_chains,
                                                  tolerance=tolerance, level=level)
                      for key, domain in self._domain.items()}
        return result

    def reset(self, level=None):
        """
        resets convergence calculation to default.

        Parameters
        ----------
        level : int, array_like of int
        """
        for key in self._keys:
            self._convergences[key].reset(level=level)
