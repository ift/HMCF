# HMCF implements Hamilton Monte Carlo Methods for the NIFTy framework
#
# Copyright(C) 2018 Max-Planck-Society, Author: Christoph Lienhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# HMCF is being developed at the Max-Planck-Institut fuer Astrophysik


import sys
import os
from typing import List
import numpy as np
import nifty4 as ift
from .convergence import *
from .utils import chain, display, Epsilon, EpsilonPowerLawDivergence, load, load_mean, load_var
from .mass import MassMain
from .logging import logger, handle_logs
import multiprocessing as mp
from multiprocessing import Process, Queue, Pipe
from multiprocessing.queues import Empty
import h5py
from datetime import datetime


__all__ = ['HMCSampler']


class HMCSampler(object):
    """ A class for Hybrid Monte Carlo (HMC) sampling

    Attributes
    ----------
    n_limits : list(int)
        a two element list defining lower and upper bound for the number of integration steps. (Default: [60, 70])
    save_burn_in_samples : bool
        whether or not samples drawn during burn-in phase are also saved to hard drive. For long burn-ins this might
        bring your file system to its knees. Default: True
    """
    def __init__(self, potential, sample_transform=None, num_processes=1,
                 sampler_dir_path=None):
        """
        Parameters
        ----------
        potential : Energy
            represents an IT-Hamiltonian. Has to live on same domain as stated in domain. The position of the potential
            defines the position where the mass operator gets probed if not provided.
        sample_transform : func, optional
            the sampling is done with fields similar to the position-field of the potential. But sometimes it might be
            convenient to have the sampled fields on another space or treated with (nonlinear) functions.
        num_processes : int
            number of processors used (and thereby markov chains started) during sampling
        sampler_dir_path : str, optional
            directory where junks of samples are saved to periodically if save_to_disk is True. (Default: a subdirectory
            called 'samples' where the __main__ script runs
        """
        # parse parameters
        self._potential = potential  # type: ift.Energy
        if sample_transform is None:
            self._sample_transform = lambda z: z
        else:
            self._sample_transform = sample_transform
        self._num_processes = num_processes
        self._display = display.LineByLineDisplay(num_processes)

        # kinda config stuff
        self._domain = self._potential.position.domain
        self._dtype = self._potential.position.dtype
        self._result_domain = self._sample_transform(self._potential.position).domain
        self._result_dtype = self._sample_transform(self._potential.position).dtype

        # other attributes, properties and related
        self._convergence = self._select_default_convergence()
        self.n_limits = [60, 70]  # defines the range of the the number of steps during leapfrog (uniform distribution)
        self._samples_count = np.zeros(self._num_processes, dtype=int)
        self._mean = self._potential.position.copy()
        self._var = None
        self._current_position = [self._potential.position.copy() for _ in range(self._num_processes)]
        self._accepted_count = np.zeros(self._num_processes, dtype=int)
        self._acceptance_rate = np.zeros(self._num_processes, dtype=float)

        # save samples to disk related stuff
        if sampler_dir_path is None:
            sampler_dir_path = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'samples')
        self._sampler_dir_path = sampler_dir_path
        try:
            os.makedirs(self._sampler_dir_path)
        except OSError:
            if not os.path.isdir(self._sampler_dir_path):
                raise
        self._h5py_file = None  # type: h5py.File
        self._h5py_file_path = None
        self._chains = [None for _ in range(self._num_processes)]  # type: List[h5py.Group]
        self.save_burn_in_samples = True

        self._epsilon = EpsilonPowerLawDivergence()
        self.mass = self._get_default_mass()

    @property
    def potential(self):
        """ift.Energy : The negative logarithm of the underlying probabilistic problem, e.g. a posterior
        distribution."""
        return self._potential

    @property
    def samples(self):
        """
        np.ndarray or dict : All samples generated after burn-in.

        If a Field was sampled, a 2+n dimensional numpy array is returned, for a MultiField it is a
        dictionary of such arrays (with appropriate keys).
        1st dimension: chain or process, 2nd dimension: index of sample
        The n other dimensions represent the field dimensions.
        """
        return load(path=self._h5py_file_path, attr_type='samples')

    @property
    def burn_in_samples(self):
        """
        np.ndarray or dict : All samples generated during burn-in.

        If a Field was sampled, a 2+n dimensional numpy array is returned, for a MultiField it is a
        dictionary of such arrays (with appropriate keys).
        1st dimension: chain or process, 2nd dimension: index of sample
        The n other dimensions represent the field dimensions.
        np.ndarray: 2+n dimensional numpy array containing fields (samples) of dimension n acquired during burn-in phase
        """
        return load(path=self._h5py_file_path, attr_type='burn_in')

    @property
    def mean(self):
        """ift.Field or ift.MultiField : mean solution (as Nifty-Field)"""
        return self._mean

    @property
    def var(self):
        """ift.Field or ift.MultiField : variance of the samples (as Nifty-Field)"""
        if self._var is None:
            self._var = load_var(path=self._h5py_file_path, domain=self._result_domain)
        return self._var

    @property
    def epsilon(self):
        """Epsilon: a child class of Epsilon, essentially defining how the integration step parameter gets adjusted"""
        return self._epsilon

    @epsilon.setter
    def epsilon(self, value):
        if isinstance(value, Epsilon):
            self._epsilon = value
        else:
            self._epsilon = value()

    @property
    def convergence(self):
        """Convergence : a child class of Convergence, essentially defining how the convergence measure is calculated.

        In case of MultiFields this property returns a MultiConvergence instance but can be set with the standard
        convergence classes such as GelmanRubinConvergence which will automatically set the convergence measure
        calculation strategy for all sub-fields in MultiField to that convergence class"""
        return self._convergence

    @convergence.setter
    def convergence(self, value):
        if isinstance(self._domain, ift.MultiDomain):
            if isinstance(value, MultiConvergence):
                self.convergence = value
            else:
                if isinstance(value, Convergence):
                    value = value.__class__
                self.convergence = {key: value(domain=domain, dtype=self._dtype[key], num_chains=self._num_processes)
                                    for key, domain in self._domain.items()}
        else:
            if isinstance(value, Convergence):
                self._convergence = value
            else:
                self._convergence = value(domain=self._domain, dtype=self._dtype, num_chains=self._num_processes)

    @property
    def sampler_dir(self):
        """str : path of directory where sample junks are saved to."""
        return self._sampler_dir_path

    @sampler_dir.setter
    def sampler_dir(self, value):
        self._sampler_dir_path = value

    @property
    def display(self):
        """Display: defines the output `style' of information during sampling. Default is a line-by-line output."""
        return self._display

    @display.setter
    def display(self, value):
        if isinstance(value, display.Display):
            self._display = value
            self._display.num_chains = self._num_processes
        else:
            self._display = value(num_chains=self._num_processes)

    @property
    def sample_transform(self):
        return self._sample_transform

    def run(self, num_samples, max_burn_in=None, convergence_tolerance=None, target_acceptance_rate=None, order=2,
            mass=None, x_initial=None):
        """
        Runs the HMC - sampling algorithm. For developers/debugging: As the markov chains run on different processors
        the actual sampling takes place in subprocesses. Since pickling the whole HMCSampler class leads to problems on
        some systems all relevant functions for multiprocessing can be found in hmc.utils and are somewhat disconnected
        from this class.
        This function only handles one end of a queue connecting the subprocesses to the main process (i.e. this
        function). The main process is only responsible for gathering samples and calculating convergence (also talking
        to the subprocesses whether they have converged or not) and creating log entries based on several parameters.

        Parameters
        ----------
        num_samples : int
            number of samples to be drawn after burn in. Default: 100.
        max_burn_in : int, optional
            maximum number of steps for the chain to converge before it is forced into sampling. If None then there is
            no restriction.
        convergence_tolerance : float
            defines a threshold for the convergence_parameter. Default: 1.
        target_acceptance_rate : float
            the targeted acceptance rate during sampling. Default: 0.8
        order : int
            The order of the symplectic integrator.
            For symmetry reasons only even integers are possible.
            order=2 equals a standard leapfrog integration.
        mass: ift.EndomorphicOperator
            mass matrix used during sampling (or until it gets reevaluated (if mass_reevaluations > 0)). Default:
            Identity.
        x_initial : ift.Field, list of ift.Field, optional
            starting point for hmc sampler, if None then a random field according to 'random_type' is generated.
            if list than len(list) has to be equal to the number of processes
        """
        # tell numpy to raise errors if something went wrong
        np_err_old_settings = np.seterr(all='raise')
        np.seterr(under='ignore')

        self.reset()
        self._generate_h5_file()

        conv_lvl = self.mass.reevaluations
        if self.mass.get_initial_mass:
            conv_lvl -= 1
        self._convergence.level = conv_lvl
        if convergence_tolerance is not None:
            self._convergence.tolerance = convergence_tolerance

        if target_acceptance_rate is not None:
            self._epsilon.target_acceptance_rate = target_acceptance_rate

        burn_in_mem = np.full(self._num_processes, True, dtype=bool)

        if mass is not None:
            self.mass.operator = mass
        chain_mass = self.mass.get_client()

        mp_function, kwargs = self._get_chain_generating_function(num_samples=num_samples, max_burn_in=max_burn_in,
                                                                  order=order, mass=chain_mass, x_initial=x_initial)
        processes, main_queue, main_transmitters = self._multiprocessing_handler(mp_function, kwargs)

        package = dict()
        # generate markov chains
        try:
            self._display.init_display()
            while any(process.is_alive() for process in processes) or (not main_queue.empty()):
                try:
                    package.update(main_queue.get(timeout=1.))
                except Empty:
                    pass  # no big deal, just avoiding a freeze if processes stopped but queue is waiting
                else:
                    if 'exception' in package:
                        raise package['exception']
                    # deal with logging related stuff
                    handle_logs(package)

                    ch_id = package['chain_identifier']
                    if package['burn_in']:
                        self._burn_in_handler(package, main_transmitters)
                    else:
                        if burn_in_mem[ch_id]:
                            burn_in_mem[ch_id] = False
                            self._finish_burn_in(chain_identifier=ch_id)
                        self._sampling_handler(package)

                    if package['new_sample']:
                        # hard drive related stuff
                        self._save_to_h5_file(package=package)

                    # give the user a clue what's happening
                    self._display.update_chain(self._convergence, package, num_samples, self._acceptance_rate[ch_id])
        except KeyboardInterrupt:
            logger.info('KeyboardInterrupt detected.')

            # try to store some kind of position if the main script wants to go on with sth despite an error
            self._mean = self._calc_emergency_mean()
        else:
            self._finish_sampling()
        finally:
            # reset console
            self._display.close()

            for process in processes:
                process.terminate()

            # reset numpy to old settings
            np.seterr(**np_err_old_settings)

            self._h5py_file.close()

            if ('traceback' in package) and (package['exception'] is not KeyboardInterrupt):
                logger.error('Error occurred in subprocess. Full traceback:\n'+str(package['traceback']))

    def _finish_burn_in(self, chain_identifier=None):
        """for classes derived from this one"""
        pass

    def _finish_sampling(self):
        self._mean = load_mean(path=self._h5py_file_path, domain=self._result_domain)

    def _calc_emergency_mean(self):
        """
        In case of an (handled) exception like KeyboardInterrupt during sampling, take the best guess of mean value
        there is.

        Returns
        -------
        mean : ift.Field
        """
        num_samples = 1
        result_field = self._sample_transform(self._current_position[0]).copy()
        for sample in self._current_position[1:]:
            num_samples += 1
            result_field += self._sample_transform(sample)
        logger.warning('Calculated mean based on last positions of chains.')
        return result_field / num_samples

    def reset(self):
        """Resets the HMCSampler to the state before a run was initiated.
        Attributes are left untouched."""
        self._mean = self._potential.position.copy()
        self._var = None
        self._h5py_file_path = None
        self._accepted_count = np.zeros(self._num_processes, dtype=int)
        self._acceptance_rate = np.zeros(self._num_processes, dtype=float)
        self._samples_count = np.zeros(self._num_processes, dtype=int)
        self._convergence.reset()

    def _generate_h5_file(self):
        run_file_name = 'run' + datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '.h5'
        self._h5py_file_path = os.path.join(self._sampler_dir_path, run_file_name)
        self._h5py_file = h5py.File(self._h5py_file_path, 'w', libver='latest')
        for i in range(self._num_processes):
            self._chains[i] = self._h5py_file.create_group('chain%04d' % i)
            sub_group = self._chains[i].create_group('samples')
            self._create_dataset(sub_group)
            if self.save_burn_in_samples:
                sub_group = self._chains[i].create_group('burn_in')
                self._create_dataset(sub_group)

    def _create_dataset(self, sub_group, raw=False):
        domain = self._result_domain
        dtype = self._result_dtype
        if raw:
            domain = self._domain
            dtype = self._dtype

        if isinstance(domain, ift.MultiDomain):
            for key, sub_domain in domain.items():
                shape = sub_domain.shape
                sub_group.create_dataset(key, shape=(0,)+shape, dtype=dtype[key],
                                         maxshape=(None,)+shape)
        else:
            shape = domain.shape
            sub_group.create_dataset('ift_field', shape=(0,)+shape, dtype=dtype, maxshape=(None,)+shape)

    def _save_to_h5_file(self, package):
        ch_id = package['chain_identifier']
        try:
            sample = self._sample_transform(package['sample'])
        except FloatingPointError:
            logger.warning('FloatingPointError occurred during sample transform. Sample was not saved.')
        else:
            if package['burn_in']:
                if self.save_burn_in_samples:
                    self._write_to_dataset(ch_id, 'burn_in', sample)
            else:
                self._write_to_dataset(ch_id, 'samples', sample)
            self._h5py_file.flush()

    def _write_to_dataset(self, chain_identifier, attr_type, sample):
        ch_id = chain_identifier
        if hasattr(sample, 'keys'):
            for key in sample.keys():
                loc_sample = sample[key]
                if isinstance(loc_sample, ift.Field):
                    loc_sample = loc_sample.val
                dataset_append(self._chains[ch_id][attr_type][key], loc_sample)
        else:
            if isinstance(sample, ift.Field):
                sample = sample.val
                dataset_append(self._chains[ch_id][attr_type]['ift_field'], sample)

    def _select_default_convergence(self):
        if isinstance(self._domain, ift.MultiDomain):
            return MultiConvergence(domain=self._domain, dtype=self._dtype, num_chains=self._num_processes)
        if self._num_processes == 1:
            return HansonConvergence(domain=self._domain, dtype=self._dtype)
        return GelmanRubinConvergence(domain=self._domain, dtype=self._dtype, num_chains=self._num_processes)

    def _get_default_mass(self):
        return MassMain(potential=self._potential, num_chains=self._num_processes)

    def _get_chain_generating_function(self, x_initial=None, **kwargs):
        """
        mainly used to slip in other generating functions if this class serves as parent

        Parameters
        ----------
        x_initial : ift.Field, list of ift.Field
        **kwargs
            additional keyword arguments for the generating function other than class attributes

        Returns
        -------
        generating_function: func
            the chain generating function
        function_kwargs: :obj:list of :obj:dict
            additional keyword arguments for said generating function
        """
        if isinstance(x_initial, list):
            size = len(x_initial)
            total_kwargs = dict(step_range=self.n_limits, epsilon=self._epsilon, **kwargs)
            generating_function_kwargs = [
                dict(chain_identifier=i, random_seed=np.random.randint(int(1.E9)), x_initial=x_initial[(i % size)],
                     **total_kwargs)
                for i in range(self._num_processes)]
        else:
            total_kwargs = dict(step_range=self.n_limits, epsilon=self._epsilon, x_initial=x_initial, **kwargs)
            generating_function_kwargs = [
                dict(chain_identifier=i, random_seed=np.random.randint(int(1.E9)), **total_kwargs)
                for i in range(self._num_processes)]
        return chain.generate_hmc_chain, generating_function_kwargs

    def _multiprocessing_handler(self, chain_generating_function, generating_function_kwargs):
        """
        Parameters
        ----------
        chain_generating_function : func
            the chain generating function executed by each process
        generating_function_kwargs : :obj:list of :obj:dict
            keyword arguments for each individual chain/process

        Returns
        -------
        processes : list of mp.Process
        main_queue : mp.Queue
        man_transmitters : list of mp.Connection
        """
        # init communication channels and processes
        main_queue = Queue(1000)
        main_transmitters = []  # type: List[mp.Connection]
        sub_receivers = []  # type: List[mp.Connection]
        for i in range(self._num_processes):
            sub_receiver, main_transmitter = Pipe(duplex=False)
            main_transmitters.append(main_transmitter)
            sub_receivers.append(sub_receiver)
        processes = [Process(name=i, target=chain_generating_function,
                             args=(main_queue, sub_receivers[i], self._potential),
                             kwargs=generating_function_kwargs[i])
                     for i in range(self._num_processes)]
        for process in processes:
            process.daemon = True
            process.start()

        return processes, main_queue, main_transmitters

    def _burn_in_handler(self, package, transmitters):
        """
        handles all main process task after communication with a chain in burn-in phase
        Parameters
        ----------
        package: dict
            queue package from subprocesses
        transmitters: :obj:list of :obj:mp.Connection
            pipe sender connections from main to subprocesses
        """
        # update convergence measure
        ch_id = package['chain_identifier']
        sample = package['sample']
        outgoing_package = dict()
        if package['dec_convergence_level']:
            self._convergence.dec_level(chain_identifier=ch_id)
        if package['new_sample']:
            try:
                self._convergence.update(sample.copy(), chain_identifier=ch_id,
                                         sample_gradient=self._potential.at(sample).gradient.copy())
            except FloatingPointError:
                self._convergence.reset()
            self._current_position[ch_id] = sample.copy()
            converged = self._convergence.converged[ch_id]
            outgoing_package.update(converged=converged, conv_level=self._convergence.level[ch_id])
            self.mass.update(incoming=package, outgoing=outgoing_package)
        else:
            outgoing_package.update(converged=False, conv_level=self._convergence.level[ch_id])

        # transmit result back to process
        transmitters[ch_id].send(outgoing_package)

    def _sampling_handler(self, package):
        ch_id = package['chain_identifier']
        if package['new_sample']:
            self._samples_count[ch_id] += 1

            # calc acceptance rate
            self._accepted_count[ch_id] += package['accepted']
            self._acceptance_rate[ch_id] = (float(self._accepted_count[ch_id]) / self._samples_count[ch_id])


def dataset_append(dataset, array):
    """
    append an n-dim numpy array to an n+1-dim h5py resizable dataset
    Parameters
    ----------
    dataset : h5py.Dataset
    array
    """
    size = dataset.shape[0]
    dataset.resize(size+1, axis=0)
    dataset[size] = array
