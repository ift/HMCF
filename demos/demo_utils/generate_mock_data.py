import numpy as np
import nifty4 as ift


__all__ = ['get_poisson_data', 'get_hierarchical_data']


class Exp3(object):
    def __call__(self, x):
        return ift.exp(3*x)

    def derivative(self, x):
        return 3*ift.exp(3*x)


def get_poisson_data(n_pixels=1024, dimension=1, length=2., excitation=1., diffusion=10.):
    """
    Parameters
    ----------
    n_pixels : int
        Number of pixels (per dimension)
    dimension : int
        Dimensionality of underlying space
    length : float
        Total physical length of regular grid.
    excitation : float
        Excitation field level.
    diffusion : float
        Diffusion constant.

    Returns
    -------
    data : ift.Field
        The poisson distributed `meausured' data field.
    s : ift.Field
        The `true' signal field (in harmonic space representation).
    R : ift.LinearOperator
        Instrument's response.
    S : ift.LinearOperator
        Correlation of signal field.
    """
    # small number to tame zero mode
    eps = 1e-8

    # Define data gaps
    na = int(0.6*n_pixels)
    nb = int(0.7*n_pixels)

    # Set up derived constants
    amp = excitation/(2*diffusion)  # spectral normalization
    pow_spec = lambda k: amp / (eps + k**2)
    pixel_width = length/n_pixels

    # Set up the geometry
    s_domain = ift.RGSpace(tuple(n_pixels for _ in range(dimension)), distances=pixel_width)
    h_domain = s_domain.get_default_codomain()
    HT = ift.HarmonicTransformOperator(h_domain, s_domain)

    # Create mock signal
    S = ift.create_power_operator(h_domain, power_spectrum=pow_spec)
    s = S.draw_sample()
    # remove zero mode
    glob = s.to_global_data()
    glob[0] = 0.
    s = ift.Field.from_global_data(s.domain, glob)

    # Setting up an exemplary response
    GeoRem = ift.GeometryRemover(s_domain)
    d_domain = GeoRem.target[0]
    mask = np.ones(d_domain.shape)
    idx = np.arange(na, nb)
    mask[np.ix_(*tuple(idx for _ in range(dimension)))] = 0.
    print(mask)
    fmask = ift.Field.from_global_data(d_domain, mask)
    Mask = ift.DiagonalOperator(fmask)
    R0 = Mask*GeoRem

    # generate poisson distributed data counts
    nonlin = Exp3()
    lam = R0(nonlin(HT(s)))
    data = ift.Field.from_local_data(d_domain, np.random.poisson(lam.local_data).astype(np.float64))

    return data, s, R0, S


def get_hierarchical_data(n_pixels=100, dimension=2, length=2., correlation_length=4., signal_to_noise=2.):
    """
    Generates a mock data set for a Wiener filter with log-normal prior.
    Implemented as d = R(exp(s)) + n where d is the mock data field, s is the 'true' signal field with a gaussian
    distribution, R the instrument response and n white noise.

    Parameters
    ----------
    n_pixels : int
        Number of pixels per dimension.
        Default: 1024
    dimension : int
        Number of dimensions
        Default: 1
    length : float
        Physical length of underlying space.
        Default: 2.
    correlation_length : float
        Default: 0.1
    signal_to_noise : float
        Amplitude of noise compared to variance of the signal.
        Default: 1.

    Returns
    -------
    d : ift.Field
        The mock data field.
    s : ift.Field
        The 'true' signal (in harmonic co-domain)
    R : ift.LinearOperator
        The instrument response (harmonic signal space -> data space)
    N : ift.DiagonalOperator
        The covariance for the noise n defined on the data domain.
    """
    shape = tuple(n_pixels for _ in range(dimension))
    pixel_width = length / n_pixels

    a = correlation_length**4

    def p_spec(k):
        return a / (k*correlation_length + 1)**4

    non_linearity = ift.library.nonlinearities.Exponential()

    # Set up position space
    s_space = ift.RGSpace(shape, distances=pixel_width)
    h_space = s_space.get_default_codomain()
    param_space = ift.UnstructuredDomain((1,))

    # Define harmonic transformation and associated harmonic space
    HT = ift.HarmonicTransformOperator(h_space, target=s_space)

    # Drawing a sample sh from the prior distribution in harmonic space
    S = ift.ScalingOperator(1., h_space)
    sh = S.draw_sample()
    p_op = ift.create_power_operator(h_space, p_spec)
    power = ift.sqrt(p_op(ift.Field.full(h_space, 1.)))
    # Choosing the measurement instrument
    mask = np.ones(s_space.shape)
    idx = np.arange(int(0.6*n_pixels), int(0.8*n_pixels))
    mask[np.ix_(*tuple(idx for _ in range(dimension)))] = 0.
    fmask = ift.Field.from_global_data(s_space, mask)
    R = ift.GeometryRemover(s_space) * ift.DiagonalOperator(fmask)

    d_space = R.target

    # Creating the mock data
    true_sky = non_linearity(HT(power*sh))
    noiseless_data = R(true_sky)
    noise_amplitude = noiseless_data.val.std() / signal_to_noise
    N = ift.ScalingOperator(noise_amplitude ** 2, d_space)
    n = N.draw_sample()

    d = noiseless_data + n

    l_c = ift.Field(domain=param_space, val=correlation_length)

    val = {'signal': power*sh, 'l_c': l_c}

    return d, ift.MultiField(val=val), R, N
