import matplotlib.pyplot as plt
import nifty4 as ift
import hmcf
import numpy as np


__all__ = ['plot_simple_hierarchical_result', 'plot_simple_hierarchical_source', 'plot_poisson_result']


def plot_simple_hierarchical_source(sl, d, R, sample_transformation):
    true_flux = sample_transformation(sl)['signal']
    fig, (x_ax, rx_ax, d_ax) = plt.subplots(nrows=1, ncols=3)
    v_min, v_max = d.min(), d.max()
    x_ax.imshow(true_flux.to_global_data(), vmin=v_min, vmax=v_max)
    x_ax.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False,
                     labelbottom=False, labelleft=False)
    x_ax.set_xlabel('x')
    rx_ax.imshow(R(true_flux).to_global_data(), cmap='viridis', vmin=v_min, vmax=v_max)
    rx_ax.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False,
                      labelbottom=False, labelleft=False)
    rx_ax.set_xlabel('R(x)')
    im = d_ax.imshow(d.to_global_data(), vmin=v_min, vmax=v_max)
    d_ax.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False,
                     labelbottom=False, labelleft=False)
    d_ax.set_xlabel('d')

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.1, 0.02, 0.8])
    fig.colorbar(im, cax=cbar_ax)


def plot_simple_hierarchical_result(sl, sampler):
    """
    Parameters
    ----------
    sl : ift.MultiField
    sampler : hmcf.HMCSampler
    """
    true_flux = sampler.sample_transform(sl)['signal']
    fig, (signal_ax, mean_ax) = plt.subplots(nrows=1, ncols=2)
    mean_val = sampler.mean['signal'].to_global_data()

    v_min, v_max = mean_val.min(), mean_val.max()
    signal_ax.imshow(true_flux.to_global_data(), vmin=v_min, vmax=v_max)
    signal_ax.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False,
                          labelbottom=False, labelleft=False)
    signal_ax.set_xlabel('x')
    im = mean_ax.imshow(mean_val, vmin=v_min, vmax=v_max)
    mean_ax.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False,
                        labelbottom=False, labelleft=False)
    mean_ax.set_xlabel('HMC mean')

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.1, 0.02, 0.8])
    fig.colorbar(im, cax=cbar_ax)

    fig, (diff_ax, std_ax) = plt.subplots(nrows=1, ncols=2)
    std_val = ift.sqrt(sampler.var['signal']).to_global_data()
    diff = abs(true_flux.to_global_data() - mean_val)

    v_min, v_max = min(std_val.min(), diff.min()), max(std_val.max(), diff.max())

    diff_ax.imshow(diff, vmin=v_min, vmax=v_max)
    diff_ax.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False,
                        labelbottom=False, labelleft=False)
    diff_ax.set_xlabel('difference')
    im = std_ax.imshow(std_val, vmin=v_min, vmax=v_max)
    std_ax.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False,
                       labelbottom=False, labelleft=False)
    std_ax.set_xlabel('HMC std')

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.1, 0.02, 0.8])
    fig.colorbar(im, cax=cbar_ax)


def plot_poisson_result(data, s, sampler):
    """
    Plots the result of the poisson demo script.
    Works only with one-dimensional problems.

    Saves the result to a pdf called 'HMC_Poisson.pdf'.

    Parameters
    ----------
    data : ift.Field
        The data vector.
    s : ift.Field
        The true signal field.
    sampler : hmcf.HMCSampler
        The sampler used for solving the problem.
    """
    hmc_mean = sampler.mean
    hmc_std = ift.sqrt(sampler.var)
    HT = ift.HarmonicTransformOperator(s.domain)
    co_dom = HT.target
    plt.clf()
    if len(co_dom.shape) > 1:
        raise TypeError('Poisson demo result plotting only possible for one-dimensional problems.')
    pixel_width = co_dom[0].distances[0]
    length = co_dom.shape[0] * pixel_width
    x = np.arange(0, length, pixel_width)
    plt.rcParams["text.usetex"] = True
    c1 = (hmc_mean-hmc_std).to_global_data()
    c2 = (hmc_mean+hmc_std).to_global_data()
    plt.scatter(x, data.to_global_data(), label='data', s=1, color='blue', alpha=0.5)
    plt.plot(x, sampler.sample_transform(s).to_global_data(), label='true sky', color='black')
    plt.fill_between(x, c1, c2, color='orange', alpha=0.4)
    plt.plot(x, hmc_mean.to_global_data(), label='hmc mean', color='orange')
    plt.xlim([0, length])
    plt.ylim([-0.1, 7.5])
    plt.title('Poisson log-normal HMC reconstruction')
    plt.legend()
    plt.savefig('HMC_Poisson.pdf')
