import nifty4 as ift
from copy import copy
import numpy as np


__all__ = ['SimpleHierarchicalEnergy']


class PowSpec(object):
    def __init__(self, field_variance):
        self._l_c = field_variance

    def __call__(self, k):
        pre_fac = self._l_c**4
        return pre_fac / (1 + self._l_c * k)**4

    def grad(self, k):
        pre_fac = - .25 * self._l_c**5
        return pre_fac / (1 + self._l_c * k)**3

    def curv(self, k):
        pre_fac = .25 * self._l_c**6
        return pre_fac / (1 + self._l_c * k)**2 / (5 + 2 * self._l_c * k)


class LogPowSpec(PowSpec):
    def __init__(self, field_variance):
        super(LogPowSpec, self).__init__(field_variance)
        self._log_l_c = np.log(self._l_c)

    def __call__(self, k):
        return 4 * (self._log_l_c - np.log(1 + self._l_c * k))

    def grad(self, k):
        return 4 * (1. / self._l_c - k / (1 + self._l_c * k))

    def curv(self, k):
        return 4 * (- 1. / self._l_c**2 + (k / (1 + self._l_c * k))**2)


class PriorEnergy(ift.Energy):
    def __init__(self, position, l_c_mu, l_c_sig2):
        """

        Parameters
        ----------
        position : ift.MultiField
            The position of the energy.
            The MultiField contains entries for the 'signal' and the correlation length 'l_c'.
        l_c_mu : float
            Hyperparameter for the log-normal distribution of l_c.
        l_c_sig2 : float
            Hyperparameter for the log-normal distribution of l_c.
        """
        super(PriorEnergy, self).__init__(position)
        self._l_c = self._position['l_c']
        self._logl_c = ift.log(self._l_c)
        self._l_c_mu = l_c_mu
        self._l_c_sig2 = l_c_sig2

    def at(self, position):
        return self.__class__(position, self._l_c_mu, self._l_c_sig2)

    @property
    @ift.memo
    def value(self):
        part = (self._logl_c.to_global_data() - self._l_c_mu)**2 / 2. / self._l_c_sig2 + self._logl_c.to_global_data()
        return part[0]

    @property
    @ift.memo
    def gradient(self):
        signal_field = self._position['signal'].copy().fill(0.)
        l_c_field = ((self._logl_c - self._l_c_mu) / self._l_c_sig2 + 1.) / self._l_c
        result_dict = {'signal': signal_field, 'l_c': l_c_field}
        return ift.MultiField(val=result_dict)

    @property
    @ift.memo
    def curvature(self):
        signal_field = self._position['signal'].copy().fill(0.)
        l_c_field = ((1. - self._logl_c + self._l_c_mu) / self._l_c_sig2 - 1.) / self._l_c**2
        diagonal = ift.MultiField(val={'signal': signal_field, 'l_c': l_c_field})
        return ift.makeOp(diagonal)


class LikelihoodEnergy(ift.Energy):
    def __init__(self, position, d, R, N, HT, inverter=None):
        """
        The likelyhood part of the full posterior energy, i.e.
        0.5 (sS^{-1}s + res N.inverse res + log(det(S)))
        where res = d - R exp(s)

        Parameters
        ----------
        position : ift.MultiField
            Containing 'signal' and 'l_c'
        d : ift.Field
            The data vector.
        R : ift.LinearOperator
            The instrument's response.
        N : ift.EndomorphicOperator
            The covariance of the noise.
        HT : ift.HarmonicTransformOperator
        """
        super(LikelihoodEnergy, self).__init__(position)
        self._d = d
        self._R = R
        self._N = N
        self._HT = HT
        self._inverter = inverter

        self._s = self._position['signal']
        self._l_c_val = self._position['l_c'].to_global_data()[0]
        self._x = ift.exp(self._HT(self._s))
        self._res = self._d - self._R.times(self._x)

        self._pow_spec = PowSpec(self._l_c_val)
        self._log_pow_spec = LogPowSpec(self._l_c_val)

        self._s_space = self._HT.domain
        self._l_c_space = self._position.domain['l_c']

    def at(self, position):
        return self.__class__(position, self._d, self._R, self._N, self._HT, self._inverter)

    def _l_c_det_part(self, power_spectrum):
        covariance = ift.create_power_operator(domain=self._s_space, power_spectrum=power_spectrum)
        return ift.Field(domain=self._l_c_space, val=.5 * covariance.times(ift.Field.full(self._s_space, val=1.)).sum())

    def _l_c_exp_part(self, power_spectrum):
        covariance = ift.create_power_operator(domain=self._s_space, power_spectrum=power_spectrum)
        return ift.Field(domain=self._l_c_space, val=.5 * self._s.vdot(covariance.inverse_times(self._s)))

    @property
    @ift.memo
    def value(self):
        S = ift.create_power_operator(domain=self._s_space, power_spectrum=self._pow_spec)
        s_part = 0.5 * (self._s.vdot(S.inverse_times(self._s)) + self._res.vdot(self._N.inverse_times(self._res)))
        return s_part + self._l_c_det_part(self._log_pow_spec).to_global_data()[0]

    @property
    @ift.memo
    def gradient(self):
        S = ift.create_power_operator(domain=self._s_space, power_spectrum=self._pow_spec)
        signal_grad = (S.inverse_times(self._s)
                       - self._HT.adjoint_times(self._x * self._R.adjoint_times(self._N.inverse_times(self._res))))
        l_c_grad = self._l_c_det_part(self._log_pow_spec.grad) + self._l_c_exp_part(self._pow_spec.grad)

        val = {'signal': signal_grad, 'l_c': l_c_grad}
        return ift.MultiField(val=val)

    @property
    @ift.memo
    def curvature(self):
        # not really the curvature but rather a metric in the context of differential manifolds
        loc_R = self._R * ift.makeOp(self._x) * self._HT
        S = ift.create_power_operator(domain=self._s_space, power_spectrum=self._pow_spec)
        signal_curv = ift.library.WienerFilterCurvature(loc_R, self._N, S, self._inverter)

        l_c_curv_diag = self._l_c_det_part(self._log_pow_spec.curv) + self._l_c_exp_part(self._pow_spec.curv)

        operators = {'signal': signal_curv, 'l_c': ift.makeOp(l_c_curv_diag)}
        return ift.BlockDiagonalOperator(operators)


class SimpleHierarchicalEnergy(ift.Energy):

    def __init__(self, position, d, R, N, HT, l_c_mu, l_c_sig2, inverter=None):
        """
        A NIFTy Energy implementing a simple hierarchical model.
        The 'signal' s has a Gaussian prior with covariance S.
        S is diagonal in the harmonic representation of s and follows a power law which is dependent on another free
        parameter, the correlation length 'l_c'.
        l_c is log-normal distributed.
        In this example, exp(s) represents the photon flux coming from the large-scale structure of the universe.

        Parameters
        ----------
        position : ift.MultiField
            Containing 'signal' and 'l_c'
        d : ift.Field
            The data vector.
        R : ift.LinearOperator
            The instrument's response.
        N : ift.EndomorphicOperator
            The covariance of the noise.
        l_c_mu : float
            Hyperparameter for the log-normal distribution of l_c.
        l_c_sig2 : float
            Hyperparameter for the log-normal distribution of l_c.   HT : ift.HarmonicTransformOperator
        inverter : ift.Minimizer
            Numerical method for inverting LinearOperators.
            This is necessary to be able to sample from the curvature which is required if the mass matrix is supposed
            to be reevaluated.
        """
        super(SimpleHierarchicalEnergy, self).__init__(position)
        self._prior = PriorEnergy(self._position, l_c_mu, l_c_sig2)
        self._likel = LikelihoodEnergy(self._position, d, R, N, HT, inverter=inverter)

    def at(self, position):
        new_energy = copy(self)
        new_energy._position = position
        new_energy._prior = self._prior.at(position)
        new_energy._likel = self._likel.at(position)
        return new_energy

    @property
    def value(self):
        return self._prior.value + self._likel.value

    @property
    def gradient(self):
        return self._prior.gradient + self._likel.gradient

    @property
    @ift.memo
    def curvature(self):
        return ift.InversionEnabler(self._prior.curvature + self._likel.curvature, inverter=self._likel._inverter)
