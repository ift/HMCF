import numpy as np
import nifty4 as ift
import hmcf
from demo_utils import *


class Exp3(object):
    def __call__(self, x):
        return ift.exp(3*x)

    def derivative(self, x):
        return 3*ift.exp(3*x)


if __name__ == "__main__":
    num_processes = 5

    np.random.seed(20)

    data, s, R, S = get_poisson_data()

    # additional needed objects
    HT = ift.HarmonicTransformOperator(s.domain)
    IC = ift.GradientNormController(name="inverter", iteration_limit=500,
                                    tol_abs_gradnorm=1e-3)
    inverter = ift.ConjugateGradient(controller=IC)
    non_lin = Exp3()

    energy = ift.library.PoissonEnergy(s, data, R, non_lin, HT, S, inverter)

    # Using an HMC sampler
    def sample_transform(z):
        return non_lin(HT(z))

    x_initial = [energy.position + 0.01*S.draw_sample() for _ in range(num_processes)]

    poisson_hmc = hmcf.HMCSampler(potential=energy, num_processes=num_processes, sample_transform=sample_transform)
    poisson_hmc.display = hmcf.TableDisplay
    poisson_hmc.epsilon = hmcf.EpsilonPowerLawDivergence
    poisson_hmc.mass.reevaluations = 2
    poisson_hmc.run(200, x_initial=x_initial)

    plot_poisson_result(data, s, poisson_hmc)
