import matplotlib.pyplot as plt
import nifty4 as ift
import numpy as np
import hmcf


np.random.seed(42)


if __name__ == "__main__":
    # hmc parameters
    num_processes = 5
    target_acceptance_rate = 0.8
    samples = 200
    max_burn_in_steps = 5000
    convergence_tolerance = 10.
    mass_reevaluations = 1

    # Set up physical constants
    # Total length of interval or volume the field lives on, e.g. in meters
    L = 2.
    # Typical distance over which the field is correlated (in same unit as L)
    correlation_length = 0.4
    # Variance of field in position space sqrt(<|s_x|^2>) (in same unit as s)
    field_variance = 2.
    # Smoothing length of response (in same unit as L)
    response_sigma = 0.01

    # Define dimension
    dimension = 2

    # Define resolution (pixels per dimension)
    N_pixels = 64

    shape = tuple(N_pixels for _ in range(dimension))

    # Set up derived constants
    k_0 = 1. / correlation_length
    # Note that field_variance**2 = a*k_0/4. for this analytic form of power
    # spectrum
    a = field_variance ** 2 / k_0 * 4.
    pow_spec = (lambda k: a / (1 + k / k_0) ** 4)
    pixel_width = L / N_pixels

    # Set up the geometry
    s_space = ift.RGSpace(shape, distances=pixel_width)
    h_space = s_space.get_default_codomain()
    HT = ift.HarmonicTransformOperator(h_space, s_space)

    # Create mock data
    Sh = ift.create_power_operator(h_space, power_spectrum=pow_spec)
    sh = Sh.draw_sample()

    R = HT * ift.create_harmonic_smoothing_operator((h_space,), 0, response_sigma)

    noiseless_data = R(sh)
    signal_to_noise = 1.0
    noise_amplitude = noiseless_data.val.std() / signal_to_noise
    N = ift.ScalingOperator(noise_amplitude ** 2, s_space)
    n = N.draw_sample()

    d = noiseless_data + n

    # Wiener filter
    j = R.adjoint_times(N.inverse_times(d))
    IC = ift.GradientNormController(iteration_limit=500, tol_abs_gradnorm=0.1)
    inverter = ift.ConjugateGradient(controller=IC)
    D = (R.adjoint * N.inverse * R + Sh.inverse).inverse
    D = ift.InversionEnabler(D, inverter)
    m = D(j)

    # hmc approach
    m0 = ift.Field(domain=m.domain, val=0.)
    energy = ift.library.WienerFilterEnergy(position=m0, d=d, R=R, N=N, S=Sh,
                                            inverter=inverter)

    wiener_hmc = hmcf.HMCSampler(energy, num_processes=num_processes, sample_transform=HT)
    # adjust some attributes
    wiener_hmc.mass.get_initial_mass = False
    wiener_hmc.mass.reevaluations = mass_reevaluations
    wiener_hmc.display = hmcf.TableDisplay
    wiener_hmc.display.level = 10

    wiener_hmc.run(num_samples=samples, max_burn_in=max_burn_in_steps, target_acceptance_rate=target_acceptance_rate,
                   convergence_tolerance=convergence_tolerance)

    hmc_m = wiener_hmc.mean

    # Plotting
    d_field = d.cast_domain(s_space)
    zmax = max(HT(sh).max(), d_field.max(), HT(m).max(), hmc_m.max())
    zmin = min(HT(sh).min(), d_field.min(), HT(m).min(), hmc_m.min())
    plotdict = {"colormap": "Planck-like", "zmax": zmax, "zmin": zmin}  # , "bbox_inches": 'tight'}

    plotting = {"mock_signal.png": HT(sh), "data.png": d_field, "conjgrad_solution.png": HT(m),
                "hmc_solution.png": hmc_m, "difference.png": abs(hmc_m - HT(m))}

    for key, val in plotting.items():
        plt.figure(0)
        plt.clf()
        plt.imshow(val.val, interpolation='none', vmin=zmin, vmax=zmax)
        plt.colorbar()
        plt.savefig(key, bbox_inches='tight')
