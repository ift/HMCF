import nifty4 as ift
import numpy as np
import hmcf


np.random.seed(42)


if __name__ == "__main__":
    # hmc settings
    num_processes = 5
    num_samples = 1000
    max_burn_in = 5000
    convergence_tolerance = 0.5
    mass_reevaluations = 3

    # geometries
    n_pixels = 1024

    noise_level = 1.
    correlation_length = 0.1

    def p_spec(k):
        return 1. / (k*correlation_length + 1) ** 4

    non_linearity = ift.library.nonlinearities.Exponential()

    # Set up position space
    s_space = ift.RGSpace(n_pixels)
    h_space = s_space.get_default_codomain()

    # Define harmonic transformation and associated harmonic space
    HT = ift.HarmonicTransformOperator(h_space, target=s_space)

    S = ift.ScalingOperator(1., h_space)

    # Drawing a sample sh from the prior distribution in harmonic space
    sh = S.draw_sample()

    # Choosing the measurement instrument
    # Instrument = SmoothingOperator(s_space, sigma=0.01)
    mask = np.ones(s_space.shape)
    mask[600:800] = 0.
    mask = ift.Field.from_global_data(s_space, mask)

    R = ift.GeometryRemover(s_space) * ift.DiagonalOperator(mask)

    d_space = R.target

    p_op = ift.create_power_operator(h_space, p_spec)
    power = ift.sqrt(p_op(ift.Field.full(h_space, 1.)))

    # Creating the mock data
    true_sky = non_linearity(HT(power*sh))
    noiseless_data = R(true_sky)
    noise_amplitude = noiseless_data.val.std()*noise_level
    #noise_amplitude = 1
    N = ift.ScalingOperator(noise_amplitude**2, d_space)
    n = N.draw_sample()
    # Creating the mock data
    d = noiseless_data + n

    IC1 = ift.GradientNormController(name="IC1", iteration_limit=100,
                                     tol_abs_gradnorm=1e-4)
    LS = ift.LineSearchStrongWolfe(c2=0.02)
    minimizer = ift.RelaxedNewton(IC1, line_searcher=LS)

    ICI = ift.GradientNormController(iteration_limit=2000,
                                     tol_abs_gradnorm=1e-3)
    inverter = ift.ConjugateGradient(controller=ICI)

    # initial guess
    m = ift.Field.full(h_space, 1e-7)
    posterior_energy = ift.library.NonlinearWienerFilterEnergy(
        m, d, R, non_linearity, HT, power, N, S, inverter=inverter)

    # Minimization with chosen minimizer
    posterior_energy = minimizer(posterior_energy)[0]
    map_solution = posterior_energy.position

    # HMC approach
    x_initial = [map_solution + S.draw_sample() for _ in range(num_processes)]

    def sample_transform(x):
        return non_linearity(HT(power*x))

    nl_hmc = hmcf.HMCSampler(potential=posterior_energy, num_processes=num_processes, sample_transform=sample_transform)
    nl_hmc.display = hmcf.TableDisplay
    nl_hmc.epsilon = hmcf.EpsilonPowerLawDivergence
    nl_hmc.mass.reevaluations = mass_reevaluations
    nl_hmc.run(num_samples=num_samples, max_burn_in=max_burn_in, convergence_tolerance=convergence_tolerance,
               x_initial=x_initial)
    hmc_mean = nl_hmc.mean
    hmc_std = ift.sqrt(nl_hmc.var)
    map_solution = sample_transform(map_solution)
    diff = abs(map_solution - hmc_mean)
    data = R.adjoint_times(d)

    lo = np.min([true_sky.min(), map_solution.min(), data.min()])
    hi = np.max([true_sky.max(), map_solution.max(), data.max()])
    plotdict = {"colormap": "Planck-like", "ymin": lo, "ymax": hi}
    ift.plot(true_sky, name="true_sky.png", **plotdict)
    ift.plot(map_solution, name="reconstructed_sky.png", **plotdict)
    ift.plot(data, name="data.png", **plotdict)
    ift.plot(hmc_mean, name='hmc_mean.png', **plotdict)
    ift.plot(diff, name='difference.png', **plotdict)
    ift.plot(hmc_std, name='hmc_std.png', colormap="Planck-like", ymin=0., ymax=hmc_std.max())
