from demo_utils import *
import numpy as np

import nifty4 as ift
import hmcf


import matplotlib.pyplot as plt
from logging import INFO, DEBUG


np.random.seed(41)


def get_ln_params(mean, mode):
    mu = np.log(mode*mean**2) / 3.
    sig2 = 2. / 3. * np.log(mean/mode)
    return mu, sig2


if __name__ == '__main__':
    ift.logger.setLevel(INFO)
    hmcf.logger.setLevel(INFO)

    # hyperparameter
    l_c_mean = 2.
    l_c_mode = .1
    l_c_mu, l_c_sig2 = get_ln_params(l_c_mean, l_c_mode)

    d, sl, R, N = get_hierarchical_data()

    s_space = sl['signal'].domain
    HT = ift.HarmonicTransformOperator(s_space)

    # energy related stuff
    ICI = ift.GradientNormController(iteration_limit=2000,
                                     tol_abs_gradnorm=1e-3)
    inverter = ift.ConjugateGradient(controller=ICI)
    energy = SimpleHierarchicalEnergy(sl, d, R, N, HT, l_c_mu, l_c_sig2, inverter=inverter)

    def sample_trafo(x):
        val = dict(x)
        val['signal'] = ift.exp(HT(val['signal']))
        return ift.MultiField(val=val)

    sampler = hmcf.HMCSampler(energy, num_processes=6, sample_transform=sample_trafo)
    sampler.display = hmcf.TableDisplay

    x_initial = [sl*c for c in [.5, .7, 1.2]]
    sampler.run(100, x_initial=x_initial)

    plot_simple_hierarchical_source(sl, d, R, sample_trafo)
    plot_simple_hierarchical_result(sl, sampler)
    plt.show()
